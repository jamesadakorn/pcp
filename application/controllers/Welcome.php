<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 function __construct() {
        parent::__construct();
        $this->load->model('M_tarad','cor');
    }
	
    public function index()
	{
		$this->load->view('include/head');
		$this->load->view('include/menu');
		$this->load->view('include/dashboard');
		$this->load->view('include/footer');
	}
	

	public function add_feature()
	{
		$data = array();
		$data['course'] = $this->cor->get_result();
		
		$this->load->view('include/head');
		$this->load->view('include/menu');
		$this->load->view('include/js');
		$this->load->view('include/add_feature',$data);
	}
	public function edit_feature($id)
	{
		$data['course'] = $this->cor->get_result();
		$data['edit'] = $this->cor->SelectIdBycor($id);
		$this->load->view('include/head');
		$this->load->view('include/menu');
		$this->load->view('include/js');
		$this->load->view('include/edit_feature',$data);
		
		
	}
	public function edit_data(){
		$id = $this->input->post('id');
		$name = $this->input->post('first_name');
    	$comment = $this->input->post('comment');
    	$price = $this->input->post('price');
        // $number = $this->input->post('number');
    	// $dis_b = $this->input->post('dis_b');
    	// $dis_per = $this->input->post('dis_per');
    	$filename = $this->input->post('img_name'); 
 
    	$data = array(
    		'fea_name' => $name,
    		'fea_des' => $comment,
    		'fea_price' => $price,
    		// 'dis_cor_b' => $dis_b,
    		// 'dis_cor_per' => $dis_per,
    		'upload_path' => 'uploads',
			'allowed_types' => "gif|jpg|png|jpeg",
			'fea_img' => $filename,
    		'fea_date' => date("Y-m-d H:i:s")
    		);
    	$this->load->library('upload', $data);
    	if($this->upload->do_upload())

				{
					
					$file_data = $this->upload->data();
					$data['fea_img'] = $file_data['file_name'];
					
					$result = $this->cor->edit_data($id,$data);
					
					// $data['message'] = "Image uploaded";
				
					$data['course'] = $this->cor->get_result();
					redirect('welcome/add_feature','refresh');
					}
				else
					{
					$result = $this->cor->edit_data($id,$data);
					$data['course'] = $this->cor->get_result();
				
		            
					redirect('welcome/add_feature','refresh');
				}
	}
	
	//  public function delete_data($id){
	// 	$id = $id ;
	// 	$this->db->where('id', $id);
 //    	$this->db->delete('id_course');
 //    	redirect('welcome/add_feature','refresh');
	// }
	public function delete_data(){
		
		$id = $this->input->post('id_delete');
		$this->cor->Delete_data($id);

	}
	public function insert(){
    			$name = $this->input->post('first_name');
		    	$comment = $this->input->post('comment');
		    	// $dis_per = $this->input->post('dis_per');
		    	$price = $this->input->post('price');
				// $dis_b = $this->input->post('dis_b');
				$filename = md5(uniqid(rand(), true));
				// $config['upload_path']='uploads';
				// $config['allowed_types']='gif|jpg|png|jpeg|pdf';
				$data = array(
					'fea_name' => $name,
		    		'fea_des' => $comment,
		    		'fea_price' => $price,
		    		// 'dis_cor_b' => $dis_b,
		    		// 'dis_cor_per' =>$dis_per,
		    		'upload_path' => 'uploads',
					'allowed_types' => 'gif|jpg|png|jpeg|pdf',
					'fea_img' => $filename,
					'fea_date' => date("Y-m-d H:i:s")
					);

				$this->load->library('upload', $data);
				if($this->upload->do_upload())
				{
					$file_data = $this->upload->data();
					$data['fea_img'] = $file_data['file_name'];
					
					$this->cor->insert($data);
					
					// $data['message'] = "Image uploaded";
				
					$data['course'] = $this->cor->get_result();
					redirect('welcome/add_feature','refresh');
					}
				else
					{
					$data = array();			
					$data['course'] = $this->cor->get_result();
					
					$error = $this->upload->display_errors();
					$data['errors'] = $error;

					redirect('welcome/add_feature','refresh');
				}
			}

    			
   
		public function update_status($id,$status)
		{
		
		
			switch ($status) {
			case '0':
			$status0 = '1';
			$data = array(
				'fea_sta' => $status0
				);
			$updateStatus = $this->cor->update_status($id,$data);

			if($updateStatus == $updateStatus){
					redirect('welcome/add_feature#slug'.$id);
				}else{
					echo 'cannot update';
				}
				break;
			case '1':
				$status1 = '0';
				$data = array(
				'fea_sta' => $status1
				);
			$updateStatus = $this->cor->update_status($id,$data);
			if($updateStatus == $updateStatus){
					redirect('welcome/add_feature#slug'.$id);
				}else{
					echo 'cannot update';
				}
				break;
			default:
				# code...
				break;
				
		
			}	
		} 



	#promotion
	public function add_promotion()
	{
		$data['course'] = $this->cor->get_result();
		$data['fea_pro'] = $this->cor->get_result_fea_pro();
		$this->load->view('include/head');
		$this->load->view('include/menu');
		$this->load->view('include/add_promotion',$data);
		$this->load->view('include/footer');
	}
	function insert_pro(){
		$data2['pro_name'] = $this->input->post('pro_name');
		$data2['pro_des'] = $this->input->post('pro_des');
		$data2['pro_start_date'] = $this->input->post('pro_start_date');
		$data2['pro_stop_date'] = $this->input->post('pro_stop_date');
		$data2['pro_count'] = $this->input->post('pro_count');
		$data2['pro_dis_b'] = $this->input->post('pro_dis_b');
		$data2['pro_dis_p'] = $this->input->post('pro_dis_p');
		$data2['pro_total_price'] = $this->input->post('pro_total_price');
		$feature = $this->input->post('feature');
		$config['upload_path'] = './image';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '100';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';
		$config['file_name'] = md5(time());
		$this->load->library('upload',$config);
		if(!$this->upload->do_upload('pro_img')){
			echo "<pre>";
			print_r($this->upload->display_errors());
			echo "</pre>";
		}else{
			$img = $this->upload->data();
			$data2['pro_img'] = $img['file_name'];
		}
		$result = $this->cor->insertPro($data2);
		if($result){
			$pro_id = $result['pro_id'];
			$data = array();
			for($i=0;$i<count($feature);$i++){
				$data[$i]['id_pro'] = $pro_id;
				$data[$i]['id_fea'] = $feature[$i];
			}
		}
		$this->cor->insertPro2($data);
		
		redirect ('welcome/add_promotion','refresh');
    }
    public function delete_pro($pro_id){
    	$sql = "DELETE Promotion,ID_Fea_Pro FROM Promotion  INNER JOIN ID_Fea_Pro ON Promotion.pro_id = ID_Fea_Pro.id_pro WHERE Promotion.pro_id = $pro_id";
    	$this->db->query($sql);
		redirect ('welcome/add_promotion');
    }
    public function select_pro($pro_id)
	{
		$data['promotion'] = $this->cor->selectById_pro($pro_id);
		$data['course'] = $this->cor->get_result();
		$this->load->view('include/head');
		$this->load->view('include/menu');
		$this->load->view('include/edit_promotion',$data);
		$this->load->view('include/footer');
	}
	public function update_pro(){
		(int)$data2['pro_id'] = $this->input->post('pro_id');
		$data2['pro_name'] = $this->input->post('pro_name');
		$data2['pro_des'] = $this->input->post('pro_des');
		$data2['pro_start_date'] = $this->input->post('pro_start_date');
		$data2['pro_stop_date'] = $this->input->post('pro_stop_date');
		$data2['pro_count'] = $this->input->post('pro_count');
		$data2['pro_dis_b'] = $this->input->post('pro_dis_b');
		$data2['pro_dis_p'] = $this->input->post('pro_dis_p');
		$data2['pro_total_price'] = $this->input->post('pro_total_price');
		$feature = $this->input->post('feature');
		$config['upload_path'] = './image';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '100';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';
		$config['file_name'] = md5(time());
		$this->load->library('upload',$config);
		if(!$this->upload->do_upload('pro_img')){
			echo "<pre>";     
			print_r($this->upload->display_errors());
			echo "</pre>";
		}else{
			$img = $this->upload->data();
			$data2['pro_img'] = $img['file_name'];
		}
		$data = array();
	 	for($i=0;$i<count($feature);$i++){
	 		$data[$i]['id_pro'] = (int)$data2['pro_id'];
	 		$data[$i]['id_fea'] = $feature[$i];
	 	}
		if($this->cor->update_pro($data2,$data)){
			redirect ('welcome/add_promotion','refresh');
		}
		die('DB error!!!!');
	}
	public function update_sta_p($pro_id,$status){
		
		
		switch ($status) {
			case '0':
			$status0 = '1';
			$data = array(
				'pro_sta' => $status0
				);
			$updateStatus = $this->cor->update_sta($pro_id,$data);

			if($updateStatus == $updateStatus){
					redirect('welcome/add_promotion#slug'.$pro_id);
				}else{
					echo 'cannot update';
				}
				break;
			case '1':
				$status1 = '0';
				$data = array(
				'pro_sta' => $status1
				);
			$updateStatus = $this->cor->update_sta($pro_id,$data);
			if($updateStatus == $updateStatus){
					redirect('welcome/add_promotion#slug'.$pro_id);
				}else{
					echo 'cannot update';
				}
				break;
			default:
				break;
		}
		
	}
 	public function front_pro(){
 		$data['course'] = $this->cor->get_result();
		$data['fea_pro'] = $this->cor->get_status();
 		$this->load->view('include/head_p');
		$this->load->view('include/body_p',$data);
		$this->load->view('include/footer_p');
 	}
 	public function select_pro_det($pro_id)
	{
		$data['course'] = $this->cor->get_result();
		$data['fea_pro'] = $this->cor->get_status();
		$data['det_pro'] = $this->cor->show_det_p($pro_id);
		$this->load->view('include/head_p');
		$this->load->view('include/detail_p',$data);
		$this->load->view('include/footer_p');
	}
	public function select_fea_det($fea_id)
	{
		$data['course'] = $this->cor->get_result();
		$data['fea_pro'] = $this->cor->get_status();
		$data['fea_det'] = $this->cor->SelectIdBycor($fea_id);
		$this->load->view('include/head_p');
		$this->load->view('include/detail_fea',$data);
		$this->load->view('include/footer_p');
	}
	public function all_pro(){
		$data['fea_pro'] = $this->cor->get_status();
		$this->load->view('include/head_p');
		$this->load->view('include/all_p',$data);
		$this->load->view('include/footer_p');
	}
	public function all_fea(){
		$data['feature'] = $this->cor->get_result();
		$data['fea_pro'] = $this->cor->get_status();
		$this->load->view('include/head_p');
		$this->load->view('include/all_fea',$data);
		$this->load->view('include/footer_p');
	}
	public function get_select_p(){
		$id = $this->input->post('id');
		$data['get_fea'] = $this->cor->get_result_fea_pro_f($id);
		print_r(json_encode($data['get_fea']));	
		}
	function insert_google() {
		$url_shop = $this->input->post('url_shop');
		$name = $this->input->post('name');
		$feature = $this->input->post('feature-all');
		$price = $this->input->post('fea_price');
		$dis_b = $this->input->post('pro_dis_b');
		$dis_p = $this->input->post('pro_dis_p');
		$pro_t = $this->input->post('pro_total_price');
		$pro_name = $this->input->post('pro_name');
		$sum = array_sum($price);

		if($_POST['pro_dis_b']==0){
			$percent = $_POST['pro_dis_p'];
		 	$subtotal = (("100" - $percent)/"100")*$sum;
		 
		
		}else{
			$subtotal = $sum - $_POST['dis_b'];
			
		}
		// print_r($_POST);
		$map_data = array(
					'entry.986956052' 	=> $url_shop,
					'entry.1716073162' 	=> $name,
					'entry.1497592144' 	=> "'".$_POST['phone'],
					'entry.2100756291' 	=> $_POST['email'],
					'entry.353144513' 	=> $pro_name,
					'entry.2020004426' 	=> implode(",", $feature),
					'entry.1252393081' 	=> $sum,
					'entry.2057835616' 	=> $pro_t,
					'entry.1010476201' 	=> $dis_b,
					'entry.1086149537' 	=> $dis_p,
					'entry.1379355263' => $subtotal
				);
				$ip = $this->input->ip_address();



				$curl = curl_init('https://docs.google.com/forms/d/e/1FAIpQLSdmGmGR2i1rHL0c1fw3I3siG2AC3P8Bvd4hhxof_0k5Dnck_Q/formResponse');
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($curl, CURLOPT_HTTPHEADER, array("REMOTE_ADDR: $ip", "HTTP_X_FORWARDED_FOR: $ip"));
				curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
				curl_setopt($curl, CURLOPT_FAILONERROR, true);
				curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_POST, sizeof($map_data));
				curl_setopt($curl, CURLOPT_POSTFIELDS, $map_data);
				curl_exec($curl);
				curl_close($curl);

		redirect ('welcome/front_pro','refresh');
	}
	//reportshop
	public function reportshop()
		{
						$data = array();
			$data['report'] = $this->cor->get_report();
			$data['fea_pro2'] = $this->cor->get_status2();
			$data['course'] = $this->cor->get_result();
			$data['promotion']= $this->cor->get_result_fea_pro();
			$data['reportshop'] = $this->cor->get_result_shop();
			$data['get_shop_pro'] = $this->cor->get_result_shop_pro();
			$data['get_shop_prox'] = $this->cor->get_result_shop_prox();
		
			$this->load->view('include/head');
			$this->load->view('include/menu'); 
			$this->load->view('include/js');
			$this->load->view('include/reportshop',$data);
		
		}
		function insert_shop(){
		$data3['shop_url'] = $this->input->post('shop_url');
		$data3['shop_owner_name'] = $this->input->post('shop_owner_name');
		$data3['shop_tel'] = $this->input->post('shop_tel');
		$data3['shop_email'] = $this->input->post('shop_email');
		$data3['shop_date'] = $this->input->post('shop_date');
		$data3['shop_pro'] = $this->input->post('shop_pro');
		$data3['shop_dis_b'] = $this->input->post('dis_b');
		$data3['shop_dis_p'] = $this->input->post('dis_p');
		$data3['shop_total_price'] = $this->input->post('pro_total');
			
		
		// echo '<pre>';
		// print_r($_POST);die;
		$fea_price = $this->input->post('fea_price');
		$fea_name= $this->input->post('fea_name');
		$feature = $this->input->post('feature');
		$total = array_sum($fea_price);
		   // if(($data3['shop_dis_b'] == 0) && ($data3['shop_dis_p'] == 0 ))
     //         {
     //            $proTotal = $data3['shop_total_price'];

     //          } else if ($data3['shop_dis_b'] == 0){
     //            $data3['shop_dis_p'];
     //             $val1 = $total - (( $total *$data3['shop_dis_p'])/100);
     //          }else{
     //                  $uu['shop_dis_b']; 
     //                   $val = $total-$data3['shop_dis_b'];
     //           }

	//	$data3['shop_total_price'] = $total;
		// $data1 = array(
		// 		'shop_url' => $shop_url,
		// 		'shop_owner_name' => $shop_owner_name,
		// 		'shop_tel'  => $shop_tel,
		// 		'shop_email' => $shop_email,
		// 		'shop_date' => $shop_date,
		// 		'shop_pro'  => $shop_pro,
		// 		'shop_dis_b' => $shop_dis_b,
		// 		'shop_dis_p' => $shop_dis_p,
		// 		'shop_total_price' => $total
		// 	);

		$result = $this->cor->insertShop($data3);
		
		if($result == $result){
				$shop_id = $result['shop_id'];
				$data = array();
				for($i=0;$i<count($feature);$i++){
				$data[$i]['id_shop'] = $shop_id;
				$data[$i]['id_fea'] = $feature[$i];
				
			}
		}	
		
	
		$this->cor->insertShop2($data);
		if($_POST['dis_b']==0){
			$percent = $_POST['dis_p'];
		 	$subtotal = (("100" - $percent)/"100")*$total;
		 
		
		}else{
			$subtotal = $total - $_POST['dis_b'];
			
		}
		// echo "<br>".$subtotal;
		// if($_POST['dis_b']!=''){
		// 	$subtotal = $total - $_POST['dis_b'];
		// }else if($_POST['dis_p']){
		// 	$percent = $_POST['dis_p'];
		// 	$subtotal = ((100 - $percent)/100)*$total;
		// }
		// echo '<pre>';
		// print_r($_POST);
		$mama = array(
					'entry.275493172' 	=> $_POST['shop_url'],
					'entry.1719558163' 	=> $_POST['shop_owner_name'],
					'entry.1754869804' 	=> "'".$_POST['shop_tel'],
					'entry.2028524781' 	=> $_POST['shop_email'] ,
					'entry.391557605' 	=> $_POST['pro_namex'],
					'entry.1795948819' 	=> $_POST['shop_date'],
					'entry.627071686' 	=> implode(",",$fea_name),
					'entry.1027608966' 	=> $total,
					// 'entry.2119687830' 	=> $total,
					'entry.2119687830'  => $subtotal,
					'entry.862697278' 	=> $_POST['pro_total'],
					'entry.771083302' 	=> $_POST['dis_b'],
					'entry.499562841' 	=> $_POST['dis_p'],
				);

				$ip = $this->input->ip_address();
				$curl = curl_init('https://docs.google.com/forms/d/e/1FAIpQLScmvDMs9O3eKG2ZYnRzyyzJh5hOMIGlCGnGnfIBJ19IefwXJQ/formResponse');
				curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
				curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($curl, CURLOPT_HTTPHEADER, array("REMOTE_ADDR: $ip", "HTTP_X_FORWARDED_FOR: $ip"));
				curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
				curl_setopt($curl, CURLOPT_FAILONERROR, true);
				curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
				curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($curl, CURLOPT_POST, sizeof($mama));
				curl_setopt($curl, CURLOPT_POSTFIELDS, $mama);
				curl_exec($curl);
				curl_close($curl);
				
		redirect ('welcome/reportshop','refresh');
 
		
		
    	}

    public function get_select2(){
		$id = $this->input->post('id');
		$data['get_fea'] = $this->cor->get_result_fea_pro2($id);
		
		
		print_r(json_encode($data['get_fea']));	
		

		}
}
