

    <div class="widget" id="widget-progress">
        <div class="widget-heading">
            Progress
        </div>
        <div class="widget-body">

            <div class="mini-progressbar">
                <div class="clearfix mb-sm">
                    <div class="pull-left">Bandwidth</div>
                    <div class="pull-right">50%</div>
                </div>
                
                <div class="progress">    
                    <div class="progress-bar progress-bar-lime" style="width: 50%"></div>
                </div>
            </div>
            <div class="mini-progressbar">
                <div class="clearfix mb-sm">
                    <div class="pull-left">Storage</div>
                    <div class="pull-right">25%</div>
                </div>
                
                <div class="progress">    
                    <div class="progress-bar progress-bar-info" style="width: 25%"></div>
                </div>
            </div>

        </div>
    </div>
</div>
                    </div>
                </div>
                <div class="static-content-wrapper">
                    <div class="static-content">
                        <div class="page-content">
                            <ol class="breadcrumb">
                                
							<li class=""><a href="index.html">Home</a></li>
							<li class="active"><a href="index.html">Dashboard</a></li>

                            </ol>
                            <div class="container-fluid">
							
							<!-- เริ่ม -->
							
							<div class="panel panel-info" style="background-color: #00ffff">
								<div class="panel-heading">
									<h2>Edit Feature</h2>
								
								</div>
								<div class="panel-editbox" data-widget-controls=""></div>
								<div class="panel-body">
								<!-- -->
									<div class="panel panel-default" data-widget='{"draggable": "false"}'>
										
										<div class="panel-editbox" data-widget-controls=""></div>
										<div class="panel-body" >
										<form action="<?php echo base_url('index.php/welcome/edit_data');?>" id="myForm" class="form-horizontal row-border" method="post" enctype="multipart/form-data">
										<?php foreach($edit as $u ){

                                     	 }  ?>       
                             
												<div class="form-group">
												<input type="hidden" name="id" value="<?php echo $u['fea_id'];?>">
													<label class="col-sm-3 control-label">หัวข้อ</label>
													<div class="col-sm-6">

														<input name="first_name" type="text" class="form-control" placeholder="" value="<?php echo $u['fea_name'];?>">
													</div>
												</div>
												<div class="form-group">
													<label for="txtarea1" class="col-sm-3 control-label">รายละเอียด</label>
													<div class="col-sm-6"><textarea name="comment" id="summernote" class="summernote"><?php echo $u['fea_des'];?></textarea></div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">ราคา</label>
													<div class="col-sm-6">

														<input name="price" type="number" class="form-control" placeholder="0.00" value="<?php echo $u['fea_price'];?>">
													</div>
												</div>
												<!-- s -->
				                      		<div class="form-group">
						                          <label class="col-sm-3 control-label" for="filebutton">รูปภาพ</label>
						                          <div class="col-md-6"> 
						                            <div class="input-group">
						                                  <span class="input-group-btn">
						                                      <span class="btn btn-default btn-file">
						                                      	<input type="hidden" name="img_name" value="<?php echo $u['fea_img']; ?>" >
						                                          Browse… <input type="file" name="userfile" id="image" required="" onchange="readURL(this)"/>
						                                      </span>
						                                  </span>
						                                  <input type="text" class="form-control addFeature-img" id="imgHolder" disabled>
						                            </div>
						                          </div>
						                          <div class="row">
														<div class="col-md-6 col-sm-6 col-xs-12">
															<div id="previewLoad" style='margin-left: 0px; display: none'>
																
															</div>
															<div class="image_preview text-right" ></div>
														</div>
													</div>
						                        </div>


										<div class="panel-footer">
											<div class="row">
												<div class="col-sm-9 col-sm-offset-4">
													<button type="submit"   class="btn btn-warning-alt" id="btn-submit"><span class="ti ti-pencil-alt"> </span>Edit</button>
													<a href="promotion_all.html"><button  type="reset" onclick="reset();" class="btn-default btn">Cancel</button></a>
												</div>
											</div>
										</div>
									</div>
									</form>
								<!-- end code add feature -->
								</div>
							</div>
							
								</div>
							</div>
						
						<!--end table add -->
								
						<!--table -->

							<!-- จบ -->
                            </div> <!-- .container-fluid -->


                        </div> <!-- #page-content -->

                    </div>
                    <footer role="contentinfo">
    <div class="clearfix">
        <ul class="list-unstyled list-inline pull-left">
            <li><h6 style="margin: 0;">&copy; 2016 Avenxo</h6></li>
        </ul>
        <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="ti ti-arrow-up"></i></button>
   	 </div>
	</footer>

<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>


<script type="text/javascript">

	$(document).ready(function(){

		$(".summernote").summernote({
			 // callbacks: {
    // 						onChange: function(contents, $editable) {
    // 							$("input#sum_desc").val(contents);
    //   							console.log('onChange:', contents, $editable);
    //   							chkform();
    // 						}
  					})					  

		});


	function readURL(input) {
	  $('#previewLoad').show();
	  if (input.files && input.files[0]) {
	    var reader = new FileReader();
	    var  name = input.files[0].name;

	    reader.onload = function (e) {
	      $('.image_preview').html('<img src="'+e.target.result+'" alt="'+reader.name+'" class="img-thumbnail" width="304" height="236"/>');
	    
	        $('.addFeature-img').val(name);
	    
	    }

	    reader.readAsDataURL(input.files[0]);
	    $('#previewLoad').hide();
	  }
	  }
	  
	  function reset(){
	  $('#image').val("");
	  $('.image_preview').html("");
	  }
	
					
		
	$(document).on('click', '#btn-submit', function(e) {
				e.preventDefault();
				swal({
						title: "ต้องการบันทึกข้อมูลใช่หรือไม่?",
						// text: 'Candidates are successfully shortlisted!',
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: '#00e600',
						confirmButtonText: 'บันทึก',
						cancelButtonText: "ยกเลิก",
						closeOnConfirm: false,
						closeOnCancel: false
				},
				function(isConfirm) {
					if (isConfirm) {
						swal({
								title: 'บันทึกข้อมูลเรียบร้อย',
								// text: 'Candidates are successfully shortlisted!',
								type: 'success'
						}, function() {
							$('#myForm').submit();
						});
					} else {
						swal("Cancelled", "", "error");
					}
				});
		});
	
					  
					  
                    

</script>