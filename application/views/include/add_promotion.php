
    <div class="widget" id="widget-progress">
    </div>
</div>
                    </div>
                </div>
                <div class="static-content-wrapper">
                    <div class="static-content">
                        <div class="page-content">
                            <ol class="breadcrumb">
                                
							<li class=""><a href="index.html">Home</a></li>
							<li class="active"><a href="index.html">Dashboard</a></li>

                            </ol>
                            <div class="container-fluid">
							
							<!-- เริ่ม -->
							
							<div class="panel panel-info" style="background-color: #00ffff">
								<div class="panel-heading">
									<h2>Add Promotion</h2>
								</div>

								<div class="panel-editbox" data-widget-controls=""></div>
								<div class="panel-body">
								<!-- -->
								
									<div class="panel panel-default" data-widget='{"draggable": "false"}'>
										
										<div class="panel-editbox" data-widget-controls=""></div>
										<div class="panel-body" >
										<form action="<?php echo base_url('index.php/welcome/insert_pro');?>" class="form-horizontal row-border" method="post" enctype="multipart/form-data">
												<div class="form-group">
													<label class="col-sm-3 control-label">หัวข้อ</label>
													<div class="col-sm-6">

														<input name="pro_name" type="text" class="form-control" placeholder="หัวข้อ Promotion" required="">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">รายละเอียด</label>
													<div class="col-sm-6">
														<textarea name="pro_des" id="summernote" class="summernote"></textarea>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">วันที่เริ่ม - วันที่สิ้นสุด</label>
													<div class="col-sm-6">
														<div class="input-group" id="period">
															<span class="input-group-addon"><i class="ti ti-calendar"></i></span>
															    <input type="text" name="pro_start_date" class="actual_range" required="">
															    <input type="text" name="pro_stop_date" class="actual_range" required="">
														</div>
													</div>
												</div>
													<div class="form-group">
														<label class="col-sm-3 control-label">Feature ที่เข้าร่วม</label>
														<div class="col-sm-6">
															<select  multiple style="width:100% !important" class="populate e2" name="feature[]" required="">
																<?php foreach ($course as $u){ ?>
																<option value="<?php echo $u['fea_id']?>"><?php echo $u['fea_name']?></option>
																<?php }?>
															</select>
														</div>
													</div>


													<div class="form-group">
														<label class="col-sm-3 control-label">จำนวน Feature ที่ให้ลูกค้าเลือก <br><font color="red">**</font>หมายเหตุ ในกรณีที่แถมโปรโมชัน <br>
													1 แถม 1 = 2 <br>,2 แถม 1 = 3</label>
															<div class="col-sm-6">
																<input name="pro_count" type="number" class="form-control" ><!-- required=""> -->
															</div>
													</div>
													<!-- <div class="form-group">
													<label class="col-sm-3 control-label">จำนวน Feature ที่ให้ลูกค้าเลือก</label>
													<div class="col-sm-6">

														<input name="pro_count" type="number" class="form-control" required="">
													</div>
													</div> -->
                                                   
                                                   	<div class="tab">
  														<button class="tablinks" onclick="openCity(event, 'baht')">ลดตามจำนวนบาท</button>
  														<button class="tablinks" onclick="openCity(event, 'percent')">ลดตามจำนวนเปอร์เซ็น</button>
  														<button class="tablinks" onclick="openCity(event, 'bahtpromo')">ราคาตามโปรโมชันหรือกำหนดราคาแถม</button>
   <!-- <button class="tablinks" onclick="openCity(event, 'Free')">กำหนดราคาแถม</button> -->
												   	</div>

													<div id="baht" class="tabcontent">
  														<div class="form-group">
						                        		<label class="col-md-3 control-label">ส่วนลดบาท : </label>  
						                       				<div class="input-group">
						                        			<input name="pro_dis_b" placeholder="0.00" class="form-control"  type="number">
						                        			</div>
						                     			</div>
						                     		</div>


													<div id="percent" class="tabcontent">
  														<div class="form-group">
						                        		<label class="col-md-3 control-label">ส่วนลด Percent : </label>	<div class="input-group">
					                       	    			<input name="pro_dis_p" placeholder="0.00" class="form-control"  type="number" max="100.00" step="0.1" min="0.00" >
				                          				  </div>
				                    		  		    </div>
				                      				</div>	


													<div id="bahtpromo" class="tabcontent">
  														<div class="form-group">
														<label class="col-sm-3 control-label">กำหนดราคา</label>
															<div class="col-sm-6">
																<input name="pro_total_price" type="number" class="form-control" placeholder="0.00" > <!-- required=""> -->
															</div>
														</div>
													</div>




												<!-- <div class="form-group">
						                        	<label class="col-md-3 control-label">ส่วนลด</label>  
						                        		<div class="col-md-4 inputGroupContainer">
						                        			<div class="col-sm-4"> 
						                        				<label class="radio-inline " for="radios-0">
						                  							<input type="radio" name="fruit" id="radios-0" >
						                            บาท
						                          				</label> 
						                          
						                       				 </div>
						                       	<div class="input-group">
						                        	<input name="pro_dis_b" placeholder="0.00" class="form-control"  type="number">
						                        </div>
						                     			</div>
						                     	</div>

												<div class="form-group">
						                        	<label class="col-md-3 control-label">ส่วนลด</label>  
						                        		<div class="col-md-4 inputGroupContainer">
						                        			<div class="col-sm-4"> 
						                        				<label class="radio-inline " for="radios-0">
																	<input type="radio" name="fruit" id="radios-0"  >
						                            Percent
																</label> 
						                        <div id="log"></div>	
						                        			</div>
					                            	<div class="input-group">
					                       	    		<input name="pro_dis_p" placeholder="0.00" class="form-control"  type="number" max="100.00" step="0.1" min="0.00" >
				                          			</div>
				                    		  			</div>
				                      			</div>	
												<div class="form-group">
													<label class="col-sm-3 control-label">ราคา</label>
													<div class="col-sm-6">

														<input name="pro_total_price" type="number" class="form-control" placeholder="0.00" required="">
													</div>
												</div>
 -->
												<!-- Picture --> 
						                        <div class="form-group">
						                          <label class="col-sm-3 control-label" for="filebutton">รูปภาพ</label>
						                          <div class="col-md-6"> 
						                            <div class="input-group">
						                                  <span class="input-group-btn">
						                                      <span class="btn btn-default btn-file">
						                                          Browse… <input type="file" name="pro_img" required="">
						                                      </span>
						                                  </span>
						                                  <input type="text" class="form-control" readonly>
						                            </div>
						                          </div>
						                          <div class="row">
						                            <div class="col-md-12">
						                              <div class="col-md-6 col-md-offset-3">
						                                <img id='img-upload'/>
						                              </div>
						                            </div>
						                          </div> 
						                        </div>
										
											<div class="form-group">
											<div class="row">
												<div class="col-sm-offset-4">
													<button data-toggle="modal" href="#myModalsa" type="button" class="btn btn-primary-alt">Save</button>
													<button type="reset" class="btn-default btn">Clear</button>
												</div>
											</div>
										</div>

										
									</div>	

										
								<!-- end code add feature -->
								</div>
							</div>
						</div>

						<!-- Modal -->
														<div class="modal fade" id="myModalsa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
															<div class="modal-dialog">
																<div class="modal-content">
																	<div class="modal-header" align="center">
																		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																		<img src="<?php echo base_url('image/Achtung.jpg')?>" class="modal-title" width="100px" heigth="100px">
																	</div>
																	<div class="modal-body" align="center">
																		<h3>ต้องการบันทึกข้อมูล Promotion ใช่หรือไม่</h3>

																	</div>
																	<div class="modal-footer">
																		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																		<button type="submit" name="submit" class="btn-primary-alt btn saveja">Save</button>
																	</div>
																</div><!-- /.modal-content -->
															</div><!-- /.modal-dialog -->
														</div><!-- /.modal -->
														</form>

<!--end code show data -->	
			<div class="col-md-13">
			<div class="panel panel-default" data-widget='{"draggable": "false"}'>
			<div class="panel-heading">
									<div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}">
                    <div id="example_filter" class="dataTables_filter pull-right"><label class="panel-ctrls-center"></label></div>
                    <i class="separator"></i><div class="dataTables_length pull-left" id="example_length"><label class="panel-ctrls-center"></label></div></div>
                    <h2>รายการ Promotion</h2>
                    </div> 

								<!-- <div class="panel-editbox" data-widget-controls=""></div>
								<div class="panel-body"> -->
								<!--code show data -->	
								<div class="row">
								<div class="col-md-12">
									
										<div class="panel-body no-padding">
									<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
										<thead>
											<tr>
												<th>ลำดับ</th>
												<th>หัวข้อ</th>
												<!-- <th scope="col" style="width: 25%;" data-tablesaw-sortable-col data-tablesaw-priority="3">รายละเอียด</th> -->
												<th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 25%;">Feature ที่เข้าร่วม</th>
												<th>ส่วนลด</th>
												<th>ราคารวม</th>
												<th><center>Pic</center></th>
												<th><center>Action</center></th>
											</tr>
										</thead>
										<?php $i = 1; foreach($fea_pro as $u ){ ?>
											<tr >
												<input type="hidden" name="" value="<?php echo $u['pro_id']?>">
												<td><p><?php echo $i++ ?></p></td>
												<td><?php echo $u['pro_name']; ?></td>
												<!-- <td><span class="feature-detail"><?php echo $u['pro_des']; ?></span></td> -->
									
												<td><?php echo $u['fea_name']?><br></td>

												<td><?php if (($u['pro_dis_b'] == 0) && ($u['pro_dis_p'] == 0)){
                                                                echo '<font color = #0099ff>ไม่มีส่วนลด</font>';
                                                          
                                                          }else if ($u['pro_dis_b'] == 0){

                                                               echo $u['pro_dis_p'].''.' %';
                                                                   
                                                          }else{
                                                               echo $u['pro_dis_b'].' บาท'; 
                                                              
                                                          } ?></td>
                                                
                                                <td><?php if($u['pro_total_price'] == 0){
                                                              echo '<font color = #0099ff>ตามจำนวนรายการ</font>';
                                                           }else{
                                                           echo $u['pro_total_price'].' บาท'; 
                                                           	} ?>
                                                           </td>

												<!-- <td><?php if($u['pro_dis_b'] == 0){
                                                         $u['pro_dis_p'].''.' %';
                                                         $val1 =$u['pro_total_price']- (($u['pro_total_price']*$u['pro_dis_p'])/100);
                                                         echo $val1.' บาท';
                                                          }else{
                                                            $u['pro_dis_b']; 
                                                            $val = $u['pro_total_price']-$u['pro_dis_b'];
                                                              echo $val.' บาท';
                                                           } ?></td>      -->                                           
                                                <td><img id= "myImg" src="<?php echo base_url('image/');?><?php echo $u['pro_img']; ?>" alt="" class="img-thumbnail" width="150px" height="150px" data-toggle="modal" data-target="#enlargeImageModal<?php echo $u['pro_id']?>"></td>
                                                
												<td><center>
													<a data-toggle="modal" href="#myModal<?php echo $u['pro_id']?>" id="<?php echo $u['pro_id']?>"><button class="btn btn-info-alt" data-toggle="tooltip" title="ดูรายละเอียดเพิ่มเติม" data-placement="bottom"><i class="fa fa-search"></i></button></a>
	                                                <?php 
	                                                  if($u['pro_sta'] == '0'){ ?>
	                                                    
	                                                    <a href="update_sta_p/<?php echo $u['pro_id'];?>/<?php echo $u['pro_sta'];?>" class="btn btn-midnightblue-alt" data-toggle="tooltip" title="ปิดการแสดง" data-placement="bottom"id = "slug<?php echo $u['pro_id'];?>"><i class="fa fa-eye-slash" name="status"></i></a>
	                                                 <?php }else{?>
	                                                    
	                                                    <a href="update_sta_p/<?php echo $u['pro_id'];?>/<?php echo $u['pro_sta'];?>" class="btn btn-success-alt" data-toggle="tooltip" title="เปิดการแสดง" data-placement="bottom"><i id = "slug<?php echo $u['pro_id'];?>" class="fa fa-eye" name="status"></i></a>
	                                                 <?php }?>
				                            		<a href="select_pro/<?php echo $u['pro_id'];?>" class="btn btn-warning-alt" data-toggle="tooltip" title="แก้ไขข้อมูล" data-placement="bottom"><i class="ti ti-pencil"></i></a>
				                           			<!-- <a href="delete_pro/<?php echo $u['id_pro'];?>" class="btn btn-danger-alt"><i class="ti ti-trash"></i></a> -->
				                           			<a data-toggle="modal" href="#myModaldel<?php echo $u['pro_id']?>" > <button class="btn btn-danger-alt" data-toggle="tooltip" title="ลบข้อมูล" data-placement="bottom"><i class="ti ti-trash"></i></button></a>
				                           			</center>
                                              	</td>
											</tr>
										 <?php } ?>
									</table>
								</div>
								<div class="panel-footer"></div>
							</div>
						</div> <!-- row -->

					</div>
				</div> 
<!--end code show data-->

							<!-- Modal -->
							<?php $i = 1; foreach($fea_pro as $u ){ ?>

<div class="modal fade" id="myModal<?php echo $u['pro_id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h2 class="modal-title">รายละเอียด ( <?php echo $u['pro_name'];?> ) </h2>
		</div>
		<div class="modal-body" >
			<?php echo $u['pro_id'];?>
			<?php echo $u['pro_des']; ?>
			<br>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close
			</button>
			
		</div>
		</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

							<div class="modal fade" id="myModaldel<?php echo $u['pro_id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header" align="center">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<img src="<?php echo base_url('image/Achtung.jpg')?>" class="modal-title" width="100px" heigth="100px">
										</div>
									<div class="modal-body" align="center">
										<h3>ต้องการลบข้อมูล Promotion ใช่หรือไม่</h3>
									</div>
										<div class="modal-footer">
											<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
											<a href="delete_pro/<?php echo $u['pro_id'];?>" class="btn btn-danger-alt">Delete</a>
										</div>
									</div><!-- /.modal-content -->
								</div><!-- /.modal-dialog -->
							</div><!-- /.modal -->

<!-- Modal images-->
							<div class="modal fade" id="enlargeImageModal<?php echo $u['pro_id']?>" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
					<div class="modal-body">
						<img src="<?php echo base_url('image/');?><?php echo $u['pro_img']; ?>" class="enlargeImageModalSource" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
							<?php }?>

							<!-- จบ -->
								</div> <!-- .container-fluid -->
							</div><!-- #page-content -->
						</div>

