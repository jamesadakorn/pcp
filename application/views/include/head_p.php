<!doctype html>
<html>
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="shortcut icon" href="<?php echo base_url();?>../tarad-promotion/favicon.ico" type="image/x-icon"/>

<title>Promotion ใหม่ๆ และ Feature ที่น่าสนใจมากมาย</title>
<meta name="Copyright" content="www.tarad.net">
<meta name="LANGUAGE" content="thai">
<meta name="ROBOTS" content="INDEX,ALL">
<meta name="Revisit-After" content="15 Days">
<meta name="description" content="หากคุณต้องการลงเมลทีละมากๆ ด้วยบริการ Email Marketing  ที่ทรงประสิทธิภาพ ส่งตรงถึงทุก Inbox ติดต่อเรา">
<meta name="keywords" content="Emaik Marketing, การตลาดอีเมล, ส่งเมลทีละมากๆ">
<meta property="og:site_name" content="TARAD.net" />
<meta property="og:locale" content="th_TH" />
<meta property="og:type" content="website" />
<meta property="og:title" content="ทำเว็บไซต์ จดโดเมนเนม ลงโฆษณา Google Facebook Instagram บริการครบวงจรโดย TARAD.net" />
<meta property="og:image" content="http://www.tarad.net/images/ogimages.jpg">
<meta property="og:type" content="article">
<meta property="og:url" content="http://www.tarad.net" />
<meta property="og:description" content="พัฒนาศักยภาพ ธุรกิจออนไลน์ ด้วยบริการครบวงจรจาก TARAD.net หาลูกค้าใหม่ด้วยเครื่องมือที่คุณเป็นเจ้าของได้ ติดต่อเรา โทรเลย  090-198-4801">

<link href="<?php echo base_url();?>../tarad-promotion/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url();?>../tarad-promotion/css/style_taradnet.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>../tarad-promotion/css/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>../tarad-promotion/css/style_promotion.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>../tarad-promotion/css/bootstrap-select.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.0/multiple-select.css">

<!-- <link href="<?php echo base_url();?>../OwlCarousel2-2.2.1/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>../OwlCarousel2-2.2.1/dist/assets/owl.carousel.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>../OwlCarousel2-2.2.1/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>../OwlCarousel2-2.2.1/dist/assets/owl.theme.default.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>../OwlCarousel2-2.2.1/dist/assets/owl.theme.green.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url();?>../OwlCarousel2-2.2.1/dist/assets/owl.theme.green.min.css" rel="stylesheet" type="text/css"> -->


<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>