<footer role="contentinfo">
    <div class="clearfix">
        <ul class="list-unstyled list-inline pull-left">
            <li><h6 style="margin: 0;">&copy; 2016 Avenxo</h6></li>
        </ul>
        <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="ti ti-arrow-up"></i></button>
    </div>
</footer>

                </div>
            </div>
        </div>
<!-- /Switcher -->
    <!-- Load site level scripts -->

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script> -->


 <script>
  $("#period").datepicker({
  inputs: $('.actual_range'),format:'yyyy/mm/dd'
  });
</script>  

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/js/jquery-1.10.2.min.js"></script> 							<!-- Load jQuery -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/js/jqueryui-1.10.3.min.js"></script> 							<!-- Load jQueryUI -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/js/bootstrap.min.js"></script> 					            <!-- Load Bootstrap -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/js/enquire.min.js"></script> 									<!-- Load Enquire -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/velocityjs/velocity.min.js"></script>					<!-- Load Velocity for Animated Content -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/velocityjs/velocity.ui.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/wijets/wijets.js"></script>     						<!-- Wijet -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/codeprettifier/prettify.js"></script> 				<!-- Code Prettifier  -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/bootstrap-switch/bootstrap-switch.js"></script> 		<!-- Swith/Toggle Button -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js"></script>  <!-- Bootstrap Tabdrop -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/iCheck/icheck.min.js"></script>     					<!-- iCheck -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/nanoScroller/js/jquery.nanoscroller.min.js"></script> <!-- nano scroller -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/js/application.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/demo/demo.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/demo/demo-switcher.js"></script>



<!-- End loading site level scripts -->
    
    <!-- Load page level scripts-->

<!-- Charts -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/charts-flot/jquery.flot.min.js"></script>             	<!-- Flot Main File -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/charts-flot/jquery.flot.pie.min.js"></script>             <!-- Flot Pie Chart Plugin -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/charts-flot/jquery.flot.stack.min.js"></script>       	<!-- Flot Stacked Charts Plugin -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/charts-flot/jquery.flot.orderBars.min.js"></script>   	<!-- Flot Ordered Bars Plugin-->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/charts-flot/jquery.flot.resize.min.js"></script>          <!-- Flot Responsive -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/charts-flot/jquery.flot.tooltip.min.js"></script> 		<!-- Flot Tooltips -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/charts-flot/jquery.flot.spline.js"></script> 				<!-- Flot Curved Lines -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/sparklines/jquery.sparklines.min.js"></script> 			 <!-- Sparkline -->


<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/switchery/switchery.js"></script>     					<!-- Switchery -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/easypiechart/jquery.easypiechart.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/fullcalendar/moment.min.js"></script> 		 			<!-- Moment.js Dependency -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/fullcalendar/fullcalendar.min.js"></script>   			<!-- Calendar Plugin -->
<!-- Sweet-Alert  --> 
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css"></script> -->
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
 -->

<!-- Sweet-Alert  --> 
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.css"></script> -->
 <!--  <script src="<?php echo base_url();?>../bootstrap/dist/sweetalert2.min.js"></script>
   <script src="<?php echo base_url();?>../bootstrap/dist/sweetalert2.js"></script> -->
    <script data-require="sweet-alert@*" data-semver="0.4.2" src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/0.4.2/sweet-alert.min.js"></script>
 <!--  <script type="text/javascript" src="<?php echo base_url();?>../bootstrap/dist/css/sweetalert2.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>../bootstrap/dist/css/sweetalert2.js"></script>  -->
<!--  <script type="text/javascript" src="<?php echo base_url();?>../bootstrap/dist/js/sweetalert2.min.js"></script>   -->

<!-- Multiselect Plugin -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-multiselect/js/jquery.multi-select.min.js"></script>

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/quicksearch/jquery.quicksearch.min.js"></script>                 <!-- Quicksearch to go with Multisearch Plugin -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-typeahead/typeahead.bundle.min.js"></script>                  <!-- Typeahead for Autocomplete -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-select2/select2.js"></script>                           <!-- Advanced Select Boxes -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-autosize/jquery.autosize-min.js"></script>                  <!-- Autogrow Text Area -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-colorpicker/js/bootstrap-colorpicker.min.js"></script>      <!-- Color Picker -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js"></script>      <!-- Touchspin -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-fseditor/jquery.fseditor-min.js"></script>                  <!-- Fullscreen Editor -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-jasnyupload/fileinput.min.js"></script>                     <!-- File Input -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>        <!-- Tokenfield -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/card/lib/js/card.js"></script>                     <!-- Card -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-daterangepicker/moment.min.js"></script>                    <!-- Moment.js for Date Range Picker -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-colorpicker/js/bootstrap-colorpicker.min.js"></script>      <!-- Color Picker -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-daterangepicker/daterangepicker.js"></script>             <!-- Date Range Picker -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>            <!-- Datepicker -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js"></script>            <!-- Timepicker -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script> <!-- DateTime Picker -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/clockface/js/clockface.js"></script>                     <!-- Clockface -->

 

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/demo/demo-pickers.js"></script>

<!-- Picture -->
<script type="text/javascript" src="<?php echo base_url();?>../bootstrap/dist/js/pic.js"></script>

<!-- Summernote -->
<!-- <script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/summernote/dist/summernote.js"></script> -->
 <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
 
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/iCheck/icheck.min.js"></script>              <!-- iCheck -->
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-inputmask/jquery.inputmask.bundle.min.js"></script>   <!-- Input Masks Plugin -->

<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/datatables/jquery.dataTables.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/demo/demo-datatables.js"></script>
<!-- Datateble -->

<!-- <script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/demo/demo-mask.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
    <script type="text/javascript">
  function readURL(input) {
  $('#previewLoad').show();
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('.image_preview').html('<img src="'+e.target.result+'" alt="'+reader.name+'" class="img-thumbnail" width="304" height="236"/>');
    }

    reader.readAsDataURL(input.files[0]);
    $('#previewLoad').hide();
  }
  }
  
  function reset(){
  $('#image').val("");
  $('.image_preview').html("");
  }
  </script>

  <script>
    $(document).ready(function() {
      $('.e2').select2()
  });
  </script>

  <script>
  $("#period").datepicker({
  inputs: $('.actual_range'),format:'yyyy/mm/dd'
  });
</script>  

 <script>
  $(document).ready(function() {
    $('.summernote').summernote({
      height: 200
    });

    $('.airmode').summernote({
      airMode: true
    });
  });
</script>
        <script>
        $(document).on('click', '#btn-submit', function(e) {
          e.preventDefault();
          swal({
            title: "ต้องการบันทึกข้อมูลใช่หรือไม่?",
            // text: 'Candidates are successfully shortlisted!',
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: '#00e600',
            confirmButtonText: 'บันทึก',
            cancelButtonText: "ยกเลิก",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function(isConfirm) {
            if (isConfirm) {
                swal({
                    title: 'บันทึกข้อมูลเรียบร้อย',
                    // text: 'Candidates are successfully shortlisted!',
                    type: 'success'
                }, function() {
                    $('#myForm').submit();
                });

            } else {
                swal("Cancelled", "", "error");
            }
        });
    });

      $(document).on('click', '.delete', function(e) {
          e.preventDefault();
          var id_delete = $(this).attr("id");  
        swal({
            title: "ต้องการลบข้อมูลใช่หรือไม่?",
            // text: "You will not be able to recover this imaginary file!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "ลบ",
            cancelButtonText: "ยกเลิก",
            closeOnConfirm: false,
            closeOnCancel: false
        },
         function(isConfirm){
           if (isConfirm) {
            $.ajax({
                url: "<?php echo base_url();?>index.php/welcome/delete_data",
                type: "POST",
                data: {'id_delete' : id_delete},
                dataType: "html",
                success: function () {
                   swal({
                    title: 'ลบข้อมูลเรียบร้อย',
                    // text: 'Candidates are successfully shortlisted!',
                    type: 'success'
                 }, function() {
                    location.reload();
                  });

                }
            });
          }else{
                swal("Cancelled", "Your imaginary file is safe :)", "error");
          } 
       });
      });
    
  //  jQuery(document).ready(function(){

  //   $('.trash').click(function () {
  //      var id_delete = $(this).attr("id");  
  //      //alert(id_delete)
  //     swal({
  //       title: "แน่ใจหรือไม่ว่าจะรูปนี้",
  //       type: "error",
  //       showCancelButton: true,
  //       cancelButtonClass: 'btn-white btn-md waves-effect',
  //       confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
  //       confirmButtonText: 'ยืนยันการลบ'
  //     }, () => {
  //       //alert(id_delete);
  //       $.ajax({
  //         method: "POST",
  //         url: "<?php echo base_url();?>index.php/welcome/delete_data",
  //         data: {
  //         'id_delete' : id_delete
  //         },  success: function () {
  //           swal({
  //             title: "บันทึกข้อมูลเรียบร้อยแล้ว",
  //             type: "success",
  //             showCancelButton: false,
  //             cancelButtonClass: 'btn-white btn-md waves-effect',
  //             confirmButtonClass: 'btn-default btn-md waves-effect waves-light',
  //           }, () => {
  //             location.reload();
  //              window.location.href = "<?php echo base_url();?>index.php/welcome/add_feature";
  //           });
  //         },
  //         error: () => {
  //           alert("Empty");
  //         }

  //       });

  //     });
  //   });
  // });


  //   function confirmDelete() {
  //       swal({
  //           title: "Are you sure?",
  //           text: "You will not be able to recover this imaginary file!",
  //           type: "warning",
  //           showCancelButton: true,
  //           confirmButtonColor: "#DD6B55",
  //           confirmButtonText: "Yes, delete it!",
  //           closeOnConfirm: false
  //       },
  //        function(isConfirm){
  //          if (isConfirm) {
  //           $.ajax({
  //               url: "scriptDelete.php",
  //               type: "POST",
  //               data: {id: 5},
  //               dataType: "html",
  //               success: function () {
  //                   swal("Done!","It was succesfully deleted!","success");
  //               }
  //           });
  //         }else{
  //               swal("Cancelled", "Your imaginary file is safe :)", "error");
  //         } 
  //      })
  //   }

            // jQuery(document).ready(function(){

              // $('a.deleteja').click(function () {
                 
              //   swal({
              //       title: "แน่ใจหรือไม่ว่าจะลบบทความนี้?",
              //       type: "error",
              //       showCancelButton: true,
              //       cancelButtonClass: 'btn-white btn-md waves-effect',
              //       confirmButtonClass: 'btn-danger btn-md waves-effect waves-light',
              //       confirmButtonText: 'ยืนยันการลบ'
              //   }), () => {
                      
              //       $.ajax({
              //           method : "POST",
              //           url    : "<?php echo base_url('index.php/welcome/delete_data') ?>",
              //           data   : {id: $(this).attr("delete")},
                        
              //           success: (data) => {
              //               swal({
              //                   title: "บันทึกข้อมูลเรียบร้อยแล้ว",
              //                   type: "success",
              //                   showCancelButton: false,
              //                   cancelButtonClass: 'btn-white btn-md waves-effect',
              //                   confirmButtonClass: 'btn-default btn-md waves-effect waves-light',
              //               }, () => {
              //                 location.reload();
              //               });
              //           },
              //           error: () => {
              //               alert("Empty");
              //           }
                
              //       });
              //   };
              // });

              // $('.saveja').click(function () {
              //   swal(
              //     'บันทึก',
              //     'บันทึกข้อมูลเรียบร้อยแล้ว',
              //     'success'
              //   )
              // });

                //visibility-on-off
              $('.btn-visibility').click(function() {
                  if ($(this).hasClass('fa-eye')) {
                      $(this).addClass('fa-eye-slash');
                      $(this).addClass('show-off');
                      $(this).removeClass('fa-eye');
                  } else {
                      $(this).addClass('fa-eye');
                      $(this).removeClass('fa-eye-slash');
                      $(this).removeClass('show-off');
                  }
              }); 

          
        </script>

        <!-- tootip -->
        <script>
         $(document).ready(function(){
         $('[data-toggle="tooltip"]').tooltip();   
         });
         </script>

         <script>
// Get the modal
var modal = document.getElementById('myModal');

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
    modal.style.display = "none";
}


$(function() {
      $('img').on('click', function() {
      $('.enlargeImageModalSource').attr('src', $(this).attr('src'));
      $('#enlargeImageModal').modal('show');
    });
});
</script>

<script>
  function openCity(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>
 									<!-- Initialize scripts for this page-->

    <!-- End loading page level scripts-->

    </body>
</html>