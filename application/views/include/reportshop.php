<div class="widget" id="widget-progress">
</div>
</div>
</div>
</div>
<div class="static-content-wrapper">
<div class="static-content">
<div class="page-content">
<ol class="breadcrumb">
	
	<li class=""><a href="index.html">Home</a></li>
	<li class="active"><a href="index.html">Dashboard</a></li>
</ol>
<div class="container-fluid">
							
							<!-- เริ่ม -->
							
							<div class="panel panel-info" style="background-color: #00ffff">
								<div class="panel-heading">
									<h2>Add Report</h2>
								
								</div>
								<div class="panel-editbox" data-widget-controls=""></div>
								<div class="panel-body">
								<!-- -->
									<div class="panel panel-default" data-widget='{"draggable": "false"}'>
										
										<div class="panel-editbox" data-widget-controls=""></div>
										<div class="panel-body" >
										
										<form action="<?php echo base_url('index.php/welcome/insert_shop');?>" id="myForm" role="form" class="form-horizontal row-border" method="post" enctype="multipart/form-data" >
												<div class="form-group">
													<label class="col-sm-3 control-label">ชื่อ URL ร้านค้า</label>
													<div class="col-sm-6">

														<input name="shop_url" type="text" class="form-control" placeholder="" required="">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">ชื่อ - สกุล</label>
													<div class="col-sm-6">

														<input name="shop_owner_name" type="text" class="form-control" placeholder="" required="">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">เบอร์โทรศัพท์</label>
													<div class="col-sm-6">

														<input name="shop_tel" type="text" class="form-control" placeholder="" required="">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">E-Mail</label>
													<div class="col-sm-6">

														<input name="shop_email" type="text" class="form-control" placeholder="" required="">
													</div>
												</div>
												
												<div class="form-group">
													<label class="col-sm-3 control-label">Promotion ที่เข้าร่วม</label>
													<div class="col-sm-6">

														<select class="form-control select" id="select" name="pro_namex">
															
																<?php foreach ($promotion as $u){ ?>
																	<option id="<?php echo $u['pro_id']?>" value="<?php echo $u['pro_name']?>"><?php echo $u['pro_name']?></option>
																		<?php }?>
																	
																						
														</select>


														<input type="hidden" name="shop_pro" id="pro_id" >

													</div>
												</div>
										
												<div class="form-group">
														<label class="col-sm-3 control-label">Feature ที่เข้าร่วม</label>
														<div class="col-sm-6" >
															<select  multiple style="width:100% !important"  class="populate e2" name="feature[]"  id="select-co-op" >								                                                              
															</select>
													
														</div>
												</div>
												  <?php $i = 1; foreach($fea_pro2 as $uu ){ ?>
										              <input  name="pro_total_price" placeholder="" class="form-control"  type="hidden" value="<?php echo $uu['pro_total_price']?>">
										              <input  name="pro_dis_b" placeholder="" class="form-control"  type="hidden" value="<?php echo$uu['pro_dis_b']?>">
										              <input  name="pro_dis_p" placeholder="" class="form-control"  type="hidden" value="<?php echo$uu['pro_dis_p']?>">
										        <?php }?>
												
												<input type="hidden" name="dis_b" id="dis_b" value="">
												<input type="hidden" name="dis_p" id="dis_p">
												<input type="hidden" name="pro_total" id="pro_total">
												<div class="form-group">
													<label for="dtp-1" class="col-sm-3 control-label">วันที่ซื้อ</label>
													<div class="col-sm-6">
										                <input class="form-control form_datetime" type="text" id="dtp-1" name="shop_date">
													</div>
												</div>
												

										
											<div class="row">
												<div class="col-sm-offset-4">
													<button type="submit"   class="btn-primary btn" id="btn-submit" >Save</button>
													<button type="reset" onclick="reset();" class="btn-default btn">Clear</button>
												</div>
											</div>
										
									
									</form>
								<!-- end code add feature -->
								</div>
			</div>
		</div>
	</div>

							



								<!--code show data -->	
								<!-- end code show data -->
			<div class="col-md-13">
			<div class="panel panel-default" data-widget='{"draggable": "false"}'>
			<div class="panel-heading">
									<div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}">
                    <div id="example_filter" class="dataTables_filter pull-right"><label class="panel-ctrls-center"></label></div>
                    <i class="separator"></i><div class="dataTables_length pull-left" id="example_length"><label class="panel-ctrls-center"></label></div></div>
                    <h2>รายการร้านค้าซื้อ Promotion</h2>
                    </div> 

								<div class="row">
								<div class="col-md-12">
									
										<div class="panel-body no-padding">
											<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
											<thead>
								<tr>
													<th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 5%;">#</th>
													<th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 15%;">ชื่อ URL</th>
													<th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 10%;">ชื่อ - สกุล</th>
													<th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 10%;">E-Mail</th>
													<!-- <th>ส่วนลด</th> -->
													<!-- <th>ราคารวม</th> -->
													<th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 10%;">เบอร์โทรศัพท์</th>
													<!-- <th>ชื่อ pro<th> -->													
													<th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 10%;"><center>ประวัติการซื้อ</center></th>
													<!-- <th><center>Action</center></th> -->
												</tr>
											</thead>
											<?php $i = 1; foreach($reportshop as $u ){

	                                             
	                                          ?>
											
												<tr>
												<input type="hidden" name="shop_id" value="<?php echo $u['shop_id']?>">
												<td><p><?php echo $i++ ?></p></td>
												<td><?php echo $u['shop_url']; ?></td>
                                                <td><?php echo $u['shop_owner_name']; ?></td>
												<td><?php echo $u['shop_email']; ?></td>
												<td><?php echo $u['shop_tel']; ?></td>
												<!-- <td value="<?php echo $u['shop_pro']?>"><?php echo $u['shop_pro'];?></td> -->
												 <td><center>
												 	<a data-toggle="modal" href="#myModal-<?php echo $u['shop_id']?>" id="<?php echo $u['shop_id']?>"><button class="btn btn-info-alt" data-toggle="tooltip" title="ดูรายละเอียดเพิ่มเติม" data-placement="bottom"><i class="fa fa-search"></i></button></a>
					                                </center></td>
												</tr>
												
											
												<?php } ?>
						</table>
					</div>
					<div class="panel-footer"></div>
				</div>
			</div>
			<!--end code show data-->
			
			<!-- -->
		</div>
	</div>


						<!--table -->
	
	<?php $i = 0; foreach ($get_shop_prox as $uu) { ?>

<div class="modal fade" id="myModal-<?php echo $uu['shop_id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	

<div class="modal-dialog" style="width :1200px;">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h2 class="modal-title">ประวัติการซื้อ Promotion : ร้านค้า Url (<?php echo $uu['shop_url']?>) </h2>
		</div>
		<div class="modal-body" >
			
			 <div class="col-md-12">
               
            <div class="panel panel-default" id="panel-inline">
                <div class="panel-heading">
                    
                    <div class="panel-ctrls"></div>
                </div>
                <div class="panel-body no-padding">
                    
                    <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered datatables" id="editable">
                        <thead>
                            <tr>
                                <th>Promotion</th>
                                <th>Feature ที่ร่วมรายการ</th>
                                <th>ราคาแต่ละ Feature</th>
                                <th>ส่วนลด</th>
                                <th>ราคารวม Promotion</th>
                                <th>ราคาที่ลูกค้าต้องจ่าย</th>
                                <th>วันที่ซื้อ</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="odd gradeX">
                                <td><?php echo $uu['pro_name']?></td>
                                <td><?php $rname =  $uu['fea_name'];
                                	$feature = explode(",", $rname);
                                //	echo $feature[0];
                                	$count = count($feature);
                                	 $count;
                                	for($y = 0; $y < $count ; $y++) {
                                		echo "<ul><li>".$feature[$y]."</li></ul>";
                                		
                             			
                                	}

                                	?>
                                </td>
                               
                                 <td><?php $rname =  $uu['fea_price'];
                                	$feature_price = explode(",", $rname);
                                //	echo $feature[0];
                                	$count = count($feature_price);
                                	 $count;
                                	for($y = 0; $y < $count ; $y++) {
                                		echo "<ul><li>".$feature_price[$y]."</li></ul>";
                                		
                                	} ?>
                                </td>
                            
                                <td><?php if (($uu['shop_dis_b'] == 0) && ($uu['shop_dis_p'] == 0)){
                                          echo '<font color = #0099ff>ไม่มีส่วนลด</font>';
                                                          
                                          }else if ($uu['shop_dis_b'] == 0){

                                           echo $uu['shop_dis_p'].''.' %';
                                                                   
                                          }else{
                                               echo $uu['shop_dis_b'].' บาท'; 
                                                              
                                           } ?></td>

                                <td><?php if($uu['shop_total_price'] == 0){
                                          echo '<font color = #0099ff>ตามจำนวนรายการ</font>';
                                          }else{
                                          echo $uu['shop_total_price'].' บาท'; 
                                          } ?> </td> 

                                    
                                <td><?php $rname =  $uu['fea_price'];
                                	$feature_price = explode(",", $rname);
                                	$total = 0;
                                //	echo $feature[0];
                                	$count = count($feature_price);
                                	 $count;
                                	for($y = 0; $y < $count ; $y++) {

                                		$total +=$feature_price[$y];
                                	}

                                   if(($uu['shop_dis_b'] == 0) && ($uu['shop_dis_p'] == 0 ))
                                     {
                                        echo $uu['shop_total_price'].' บาท';

                                     } else if ($uu['shop_dis_b'] == 0){
                                     	$uu['shop_dis_p'].''.' %';
                                                         $val1 = $total - (( $total *$uu['shop_dis_p'])/100);
                                                         echo $val1.' บาท';
                                     }else{
                                                            $uu['shop_dis_b']; 
                                                            $val = $total-$uu['shop_dis_b'];
                                                              echo $val.' บาท';
                                       }

                                	?>
                                </td>

                                <!--  if($uu['shop_dis_b'] == 0 ){
                                                         $uu['shop_dis_p'].''.' %';
                                                         $val1 = $total - (( $total *$uu['shop_dis_p'])/100);
                                                         echo $val1.' บาท';
                                                          }else if ($uu['shop_dis_p'] == 0) {
                                                            $uu['shop_dis_b']; 
                                                            $val = $total-$uu['shop_dis_b'];
                                                              echo $val.' บาท';
                                                           } else if (($uu['shop_dis_b'] == 0) && ($uu['shop_dis_p'] == 0 ))
                                                             {
                                                           	  echo $uu['shop_total_price'];
                                                             }

                                	?> -->

                              <!--    <td> <?php if($uu['shop_dis_b'] == 0){
                                                         $uu['shop_dis_p'].''.' %';
                                                         $val1 =$uu['shop_total_price']- (($u['shop_total_price']*$uu['shop_dis_p'])/100);
                                                         echo $val1.' บาท';
                                                          }else{
                                                            $uu['shop_dis_b']; 
                                                            $val = $uu['shop_total_price']-$uu['pro_dis_b'];
                                                              echo $val.' บาท';
                                                           } ?></td>  -->



                                <td><?php echo $uu['shop_date']?></td>

                            </tr>
                           
                        </tbody>
                    </table><!--end table-->
                </div>
                
            </div>
        </div>
			<br>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			
		</div>
		</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
<?php 
}?>

							<!-- จบ -->
                            </div> <!-- .container-fluid -->


                        </div> <!-- #page-content -->

                    </div>

                    <script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-select2/select2.min.js"></script>
					<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-select2/select2.js"></script>    

					<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-daterangepicker/moment.min.js"></script>              			<!-- Moment.js for Date Range Picker -->

					<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-colorpicker/js/bootstrap-colorpicker.min.js"></script> 			<!-- Color Picker -->

					<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-daterangepicker/daterangepicker.js"></script>     				<!-- Date Range Picker -->
					<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.js"></script>      			<!-- Datepicker -->
					<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.js"></script>      			<!-- Timepicker -->
					<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script> <!-- DateTime Picker -->

					<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/clockface/js/clockface.js"></script>     								<!-- Clockface -->


					<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/demo/demo-pickers.js"></script>

					<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-select2/select2.js"></script>  
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/datatables/jquery.dataTables.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/demo/demo-datatables.js"></script>
<!-- Datateble -->


                 <footer role="contentinfo">
				    <div class="clearfix">
				        <ul class="list-unstyled list-inline pull-left">
				            <li><h6 style="margin: 0;">&copy; 2016 Avenxo</h6></li>
				        </ul>
				        <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="ti ti-arrow-up"></i></button>
				    </div>
				</footer>  


<script type="text/javascript">
	$('.e2').select2();
	
	


    $('#select').on('change', function() {

        //$("#area_select").html(data);
        	var pro_id = $("#select option:selected").attr("id");
        	//console.log(pro_id)
            $.ajax({
                  type:"POST",
                  dataType: 'JSON',
                  url:"<?php echo base_url('index.php/welcome/get_select2') ?>",
                  data: {id:pro_id},
                  success: function(data) {
                  	console.log(data);
                  	var isData = data.fea_name;
                  	var isdis_b = data.dis_b;
                  	var isdis_p = data.dis_p;
                  	var ispro_total = data.pro_total;
                  	var html = "";
                  	var thisData = isData.split(",");
                  	var price = data.fea_price;
                  	var thisPrice = price.split(",");
                  	var isId= data.fea_id;
                  	var thisId = isId.split(",");
                  	$('input#dis_b').val(isdis_b);
                  	$('input#dis_p').val(isdis_p);
                  	$('input#pro_total').val(ispro_total);
                  	//console.log(thisData);
                  	 console.log('-----------')
                  	 console.log(isData);
                  	 console.log('-----------')
                  //	 console.log(thisId);  
                  //	console.log(isId)
                  	$("#pro_id").attr("value",pro_id)
                  	$("select#select-co-op").html(" ");              	
			         for(var i = 0 ; i<thisData.length ; i++){
			         	
			         var appendTo  = '<option data-fea="'+thisData[i]+'" data-price="'+thisPrice+'" value ="'+thisId[i]+'">'+thisData[i]+'</option>';
					  // class="get_id" id="'+thisId[i]+'"
                      $('select#select-co-op').append(appendTo);

                   		
			         }
			         
			         $("#select-co-op").on('change keyup',function(){
                            $('input.fea_price').remove();

                            var val = $(this).val()
                            for(var i = 0;i<val.length;i++){	
                              var price = $("#select-co-op").find("option[value='"+val[i]+"']").data('price')
                              console.log(price)
                        	  var append  = '<input type="hidden" name="fea_price[]" class="fea_price" value="'+thisPrice[i]+'">';
                        	  var appendw  = '<input type="hidden" name="fea_name[]" class="fea_price" value="'+thisData[i]+'">';
                              $("form#myForm").append(append)
                              $("form#myForm").append(appendw)

                            }
                    })

                  }
                });

            $("select#select-co-op").select2("val", "");

       
      });



   
</script>
