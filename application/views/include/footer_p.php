<footer>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-4 col-md-4">
        <h4>เกี่ยวกับเรา</h4>
        <p>TARAD.net บริการของเครือบริษัท TARAD.com ที่เห็นความสำคัญของการพัฒนาเว็บไซต์ โฆษณาดิจิทัล และความรู้เกี่ยวกับธุรกิจออนไลน์ เพื่อเพิ่ม ยอดขาย และโปรโมทธุรกิจตามความต้องการของกลุ่มลูกค้าไม่ว่าขนาดเล็ก หรือใหญ่ด้วยบริการครบวงจร</p>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4">
        <h4>สินค้าและบริการ</h4>
        <div class="row">
          <ul>
            <li class="col-xs-6 col-sm-6 col-md-6"><a href="webecommerce.html">E-Commerce Website</a></li>
            <li class="col-xs-6 col-sm-6 col-md-6"><a href="domainname.html">Domain Name</a></li>
            <li class="col-xs-6 col-sm-6 col-md-6"><a href="facebookads.html">Facebook Ads</a></li>
            <li class="col-xs-6 col-sm-6 col-md-6"><a href="instagramads.html">Instagram Ads</a></li>
            <li class="col-xs-6 col-sm-6 col-md-6"><a href="googleapps.html">Google Apps for Work</a></li>
            <li class="col-xs-6 col-sm-6 col-md-6"><a href="emailmarketing.html">E-Mail Marketing</a></li>
            <li class="col-xs-6 col-sm-6 col-md-6"><a href="googleadwords.html">Google Adwords</a></li>
            <li class="col-xs-6 col-sm-6 col-md-6"><a href="promotion.html">Promotion</a></li>
          </ul>
        </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4">
        <h4>ติดต่อสอบถาม <i class="ico ico-fb"></i><i class="ico ico-line"></i></h4>
        <ul>
          <li><i class="ico ico-tel"></i><span><a href="tel:02-515-2333">02-515-2333</a></span></li>
          <li><i class="ico ico-address"></i><span>อาคารอีฟราสตรัคเจอร์ เลขที่ 522 ถนนรัชดาภิเษก แขวงสามเสนนอก เขตห้วยขวาง จังหวัดกรุงเทพมหานคร 10310</span></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="copyright">Copyright &copy; 2016 TARAD.net All Right Reserved.</div>
</footer>
<!--<p id="backtop"> <a href="#top"><span></span></a> </p>--> 
<script src="<?php echo base_url();?>../tarad-promotion/js/jquery.min.js"></script> 
<script src="<?php echo base_url();?>../tarad-promotion/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url();?>../tarad-promotion/js/owl.carousel.js"></script> 
<script src="<?php echo base_url();?>../tarad-promotion/js/taradnetscript.js"></script> 
<script src="<?php echo base_url();?>../tarad-promotion/js/bootstrapValidator.js"></script> 
<script src="<?php echo base_url();?>../tarad-promotion/js/bootstrap-select.js"></script>
<!-- <script src="<?php echo base_url();?>../OwlCarousel2-2.2.1/dist/owl.carousel.js"></script>
<script src="<?php echo base_url();?>../OwlCarousel2-2.2.1/dist/owl.carousel.min.js"></script> -->

 


<script src="https://cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.0/multiple-select.js"></script>


<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-2511724-33', 'auto');
 ga('send', 'pageview');

</script>
<script type="text/javascript">
  $('.selectpicker').selectpicker();
  



    $('#select-pro').on('change', function() {

        //$("#area_select").html(data);
          var pro_id = $("#select-pro").val();
          //console.log(pro_id);
            $.ajax({
                  type:"POST",
                  dataType: 'JSON',
                  url:"<?php echo base_url('index.php/welcome/get_select_p') ?>",
                  data: {id:pro_id},
                  success: function(data) {
                    console.log(data);
                    var isData = data.fea_name;
                    var p_name = data.pro_name;
                    var price = data.fea_price;
                    var dis_p = data.pro_dis_p;
                    var dis_b = data.pro_dis_b;
                    var pro_t = data.pro_total_price;
                    var proData = data.pro_count;
                    var thisData = isData.split(",");
                    var thisPrice = price.split(",");
                    console.log(pro_t)
                    console.log(dis_b)
                    console.log(dis_p)
                    console.log(p_name)
                    console.log(thisPrice)
                    console.log(thisData)   
                    $("select#select-fea").html(" ");               
                    for(var i = 0 ; i<thisData.length ; i++){
                      console.log(thisData[i]);
                      var appendTo  = '<option data-price="'+thisPrice[i]+'" value="'+thisData[i]+'">'+thisData[i]+'</option>';
                      $('select#select-fea').selectpicker('destroy');
                      $('select#select-fea').append(appendTo);
                      $('select#select-fea').data('max-options',proData);
                      $('select#select-fea').selectpicker();
                    }
                    $("#select-fea").on('change keyup',function(){
                            $('input.fea_price').remove();

                            var val = $(this).val()
                            for(var i = 0;i<val.length;i++){
                              var price = $("#select-fea").find("option[value='"+val[i]+"']").data('price')
                              console.log(price)
                        var append  = '<input type="hidden" name="fea_price[]" class="fea_price" value="'+price+'">';
                        var append_dis_b = '<input type="hidden" name="pro_dis_b" class="pro_dis_b" value="'+dis_b+'">';
                        var append_dis_p = '<input type="hidden" name="pro_dis_p" class="pro_dis_p" value="'+dis_p+'">';
                        var append_pro_t = '<input type="hidden" name="pro_total_price" class="pro_total_price" value="'+pro_t+'">';
                        var append_pro_name = '<input type="hidden" name="pro_name" class="pro_name" value="'+p_name+'">';
                              $("form#submitPromotion").append(append)
                              $("form#submitPromotion").append(append_dis_b)
                              $("form#submitPromotion").append(append_dis_p)
                              $("form#submitPromotion").append(append_pro_t)
                              $("form#submitPromotion").append(append_pro_name)
                            }
                    })


                  }
                });

           

       
      });

          $('.carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));
  
  for (var i=0;i<2;i++) {
    next=next.next();
    if (!next.length) {
        next = $(this).siblings(':first');
    }
    
    next.children(':first-child').clone().appendTo($(this));
  }
});

      //$("input[name='fea_price']").val($("select#select-fea option[0]:selected").attr('price'))

// $(document).ready(function(){
//   $('.owl-carousel').owlCarousel();
// });


//    var owl = $('.owl-carousel');
// owl.owlCarousel({
//     items:4,
//     loop:true,
//     margin:10,
//     autoplay:true,
//     autoplayTimeout:1000,
//     autoplayHoverPause:true
// });
// $('.play').on('click',function(){
//     owl.trigger('play.owl.autoplay',[1000])
// })
// $('.stop').on('click',function(){
//     owl.trigger('stop.owl.autoplay')
// })




</script>
</body>
</html>
