<body>
<header>
  <div class="contact-top">
    <div class="container">
      <div class="row">
        <div class="contact-top-inner"> <i class="ico ico-teltop"></i><span>ติดต่อสอบถาม: <a href="tel:02-515-2333">02-515-2333</a></span></div>
      </div>
    </div>
  </div>
  <div class="navbar navbar-inverse" role="navigation" id="slide-nav">
    <div class="container">
      <div class="navbar-header"> <a class="navbar-toggle"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </a> <a class="navbar-brand" href="http://www.tarad.net"><img src="<?php echo base_url();?>../tarad-promotion/images/logo-taradnet.png" alt="TARAD.net Empower   E-Business   Extraordinary"/></a> </div>
      <div id="slidemenu">
        <ul class="nav navbar-nav">
          <li><a href="index.html">หน้าแรก</a></li>
          <li class="active dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">สินค้าและบริการ <span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">              <li><a href="webecommerce.html" class="icon"><span class="ico-top ico-shopping"></span>E-Commerce Website</a></li>
              <li><a href="domainname.html" class="icon"><span class="ico-top ico-domain"></span>Domain Name</a></li>
              <li><a href="googleapps.html" class="icon"><span class="ico-top ico-googleapp"></span>Google Apps for Work</a></li>
              <li><a href="googleadwords.html" class="icon"><span class="ico-top ico-google"></span>Google Adwords</a></li>
              <li><a href="facebookads.html" class="icon"><span class="ico-top ico-facebook"></span>Facebook Ads</a></li>
              <li><a href="instagramads.html" class="icon"><span class="ico-top ico-instagram"></span>Instagram Ads</a></li>
              <li><a href="emailmarketing.html" class="icon"><span class="ico-top ico-email"></span>E-Mail Marketing</a>
              <li><a href="<?php echo base_url('index.php/welcome/front_pro');?>"><span class="ico-top ico-shopping"></span>Promotion</a></li>
              </li>
            </ul>
          </li>
          <li><a href="ebs/index.html">อบรมและสัมมนา</a></li>
          <li><a href="payment.html">วิธีการชำระเงิน</a></li>
          <li><a href="abouttaradnet.html">เกี่ยวกับเรา</a></li>
          <li><a href="contactus.html">ติดต่อเรา</a></li>
        </ul>
      </div>
    </div>
  </div>
</header>
<div class="maincontent googleapps-page">
  <section class="main-slider">
    <div class="cover-img"><img src="<?php echo base_url();?>../tarad-promotion/images/promotion.png" alt="E-Mail Marketing สร้างทางเลือกในการโกยกำไรกับสื่อออนไลน์แนวใหม่"/></div>
  </section>
  
  <section class="main-slider">
    <?php $j = 1; foreach($det_pro as $u ){ ?>
    <input type="hidden" name="pro_id" value="<?php echo $u['pro_id']?>">
    <h1 class="text-red2 style-text"><center>Promotion <?php echo $u['pro_name']?></center></h1><br><br>
    <center><div class="imgpro" style="width: 37%;"><img src="<?php echo base_url('image/');?><?php echo $u['pro_img']; ?>" width="500px" height="550px">
    </div><br><br><br>

    <center>
      <div><h2 class=" text-red2 text-topic-red2">รายละเอียด</h2>
              <h4><?php echo $u['pro_des']?></h4>
            <h2 class="text-red2 text-topic-red2">Feature</h2>
              <h4><?php echo $u['fea_name']?></h4>
      </div>
    </center>
    <?php } ?>
  </section>


<div class="bg-gray">
          <div class="container page-title">
                  <div class="row">
                      <div class="col-md-9">
                           <h3 class="text-red2 style-text media-left">Feature อื่น ๆ </h3><br>
                      </div>

                      <div class="col-md-3"> 
                      <div class="btn-form">
                        <a href="<?php echo base_url('index.php/welcome/all_fea');?>"><input type="image" src="<?php echo base_url();?>../tarad-promotion/images/viewmore.png" alt="Submit" width="155" height="55" ></a>
                      </div>
                      </div> 
                    </div>
                  
                 


              <div id="carousel-example" class="carousel slide hidden-xs" data-ride="carousel">

                      <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                      <div class="item active" >
                        </div>

                       <?php $j = 1; foreach($course as $u ){ ?>
                          <div class="item">
                                  <div class="col-sm-3">
                                      <div class="col-item polaroid bg-white">
                                          <div class="photo "> 
                                              <img src="<?php echo base_url()."uploads/".$u['fea_img']; ?>" width="225px" height="200px">
                                          </div>
                                          <div class="info ">
                                              <div class="row">
                                                  <div class="price col-md-12">
                                                      <h5><?php echo $u['fea_name']; ?></h5>
                                                      <h5 class="price-text-color">
                                                          <?php echo $u['fea_price']?>
                                                      </h5>
                                                  </div>

                                                  <div class="rating hidden-sm col-md-6">
                                                      <i class="price-text-color fa fa-star"></i><i class="price-text-color fa fa-star">
                                                      </i><i class="price-text-color fa fa-star"></i><i class="price-text-color fa fa-star">
                                                      </i><i class="fa fa-star"></i>
                                                  </div>
                                              </div>
                                              <div class="separator clear-left">
                                                  <p class="btn-details">
                                                      <i class="fa fa-list"></i><a href="<?php echo base_url('index.php/welcome/select_fea_det');?>/<?php echo $u['fea_id']?>" class="hidden-sm">More details</a>
                                              </div>
                                              <div class="clearfix">
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  </div>
                                  
                                  <?php }?>

                          <!-- Controls -->
                        </div> <!-- carousel-inner -->
          
                          
                              <a class="left carousel-control" href="#carousel-example" data-slide="prev"> 
                                <span class="glyphicon glyphicon-chevron-left"></span>
                                <span class="sr-only">Previous</span>
                              </a>
                              <a class="right carousel-control" href="#carousel-example" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                                <span class="sr-only">Next</span>
                              </a>
              </div> <!-- carousel slide -->

          </div> <!-- row -->
        </div> <!-- container page-title -->
      </div> <!-- bg -->


<div class="call-info">
  <div class="container">
    <div class="row"> <span class="text-realred">สนใจบริการ สามารถกรอกข้อมูลด้านล่างได้เลย!!</span></div>
  </div>
</div>
<div id="contact-form">
  <div class="container">
    <div class="contact-wrap">
      <h2 class="title-form">สนใจ Promotion กรอกเลย <span>เพียงกรอกรายละเอียด</span> <i>ฟรี!!</i></h2>
      <form class="form-horizontal" role="form" action="<?php echo base_url('index.php/welcome/insert_google');?>" method="post"  id="submitPromotion">
        <div class="form-group">
          <label class="col-sm-3 control-label">ชื่อ URL ร้านค้า<i>*</i> :</label>
          <div class="col-sm-9 inputGroupContainer">
            <input  name="url_shop" placeholder="ชื่อ url" class="form-control"  type="text">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label">ชื่อ-นามสกุล<i>*</i> :</label>
          <div class="col-sm-9 inputGroupContainer">
            <input  name="name" placeholder="ชื่อ-นามสกุล" class="form-control"  type="text">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label">เบอร์โทรศัพท์<i>*</i> :</label>
          <div class="col-sm-9 inputGroupContainer">
            <input  name="phone" placeholder="เบอร์โทรศัพท์" class="form-control"  type="text">
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-3 control-label">อีเมล<i>*</i> :</label>
          <div class="col-sm-9 inputGroupContainer">
            <div class="input-group"> <span class="input-group-addon">@</span>
              <input  name="email" placeholder="อีเมล" class="form-control"  type="text">
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">เลือก Promotion ที่สนใจ<i>*</i> :</label>
          <div class="col-md-9 selectContainer">
            <select name="promotion" class="form-control selectpicker" title="กรุณาเลือกPromotionที่สนใจ" id="select-pro">
              <?php $i = 1; foreach($fea_pro as $u ){ ?>
              <option value="<?php echo $u['pro_id']?>"><?php echo $u['pro_name']; ?></option>
              <?php }?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-3 control-label">เลือก Feature ที่เข้าร่วม<i>*</i> :</label>
          <div class="col-md-9 selectContainer">
            <select name="feature-all[]" class="form-control selectpicker 2" multiple="" title="กรุณาเลือกPromotionที่สนใจ" id="select-fea">
            </select>
          </div>
        </div>
        <div class="form-group">
          <div class="btn-form">
            <button type="submit" class="btn btn-primary">ส่ง</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>