<div class="widget" id="widget-progress">
</div>
</div>
</div>
</div>
<div class="static-content-wrapper">
<div class="static-content">
<div class="page-content">
<ol class="breadcrumb">
	
	<li class=""><a href="index.html">Home</a></li>
	<li class="active"><a href="index.html">Dashboard</a></li>
</ol>
<div class="container-fluid">
	
	<!-- เริ่ม -->
	
	<div class="panel panel-info" style="background-color: #00ffff">
		<div class="panel-heading">
			<h2>Add Feature</h2>
			
		</div>
		<div class="panel-editbox" data-widget-controls=""></div>
		<div class="panel-body">
			<!-- -->
			<div class="panel panel-default" data-widget='{"draggable": "false"}'>
				
				<div class="panel-editbox" data-widget-controls=""></div>
				<div class="panel-body" >
					
					<form action="<?php echo base_url('index.php/welcome/insert');?>" id="myForm" class="form-horizontal row-border" method="post" enctype="multipart/form-data">
						<div class="form-group">
							<label class="col-sm-3 control-label">หัวข้อ</label>
							<div class="col-sm-6">
								<input name="first_name" type="text" class="form-control addFeature" placeholder="หัวข้อ feature" required="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-3 control-label">รายละเอียด</label>
							<div class="col-sm-6">
								<textarea name="comment" id="summernote" class="summernote"></textarea>
								<input type="hidden" id="sum_desc">
							</div>
						</div>
						
						
						
						
					
							       <div class="form-group">
						                          <label class="col-sm-3 control-label" for="filebutton">รูปภาพ</label>
						                          <div class="col-md-6"> 
						                            <div class="input-group">
						                                  <span class="input-group-btn">
						                                      <span class="btn btn-default btn-file">
						                                          Browse… <input type="file" name="userfile" id="image" required="" onchange="readURL(this)"/>
						                                      </span>
						                                  </span>
						                                  <input type="text" class="form-control addFeature-img" id="imgHolder" disabled>
						                            </div>
						                          </div>
						                          <div class="row">
														<div class="col-md-6 col-sm-6 col-xs-12">
															<div id="previewLoad" style='margin-left: 0px; display: none'>
																
															</div>
															<div class="image_preview text-right" ></div>
														</div>
													</div>
						             </div>
							
							<div class="form-group">
							<label class="col-sm-3 control-label">ราคา</label>
							<div class="col-sm-6">
								<input name="price" type="number" class="form-control addFeature" placeholder="0.00" required="">
							</div>
						</div>

							
							<div class="row">
								<div class="col-sm-offset-4">
									<button type="submit"   class="btn-primary btn" id="btn-submit" disabled="disabled">Save</button>
									<button type="reset" onclick="reset();" class="btn-default btn">Clear</button>
								</div>
							</div>
						
						
					</form>
					<!-- end code add feature -->
				</div>
			</div>
		</div>
	</div>



<!-- end code show data -->
			<div class="col-md-13">
			<div class="panel panel-default" data-widget='{"draggable": "false"}'>
			<div class="panel-heading">
									<div class="panel-ctrls" data-actions-container="" data-action-collapse="{&quot;target&quot;: &quot;.panel-body&quot;}">
                    <div id="example_filter" class="dataTables_filter pull-right"><label class="panel-ctrls-center"></label></div>
                    <i class="separator"></i><div class="dataTables_length pull-left" id="example_length"><label class="panel-ctrls-center"></label></div></div>
                    <h2>รายการ Feature </h2>
                    </div> 

								<div class="row">
								<div class="col-md-12">
									
										<div class="panel-body no-padding">
											<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
											<thead>
								<tr>
									<th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 5%;"><center>ลำดับ</center></th>
									<th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 15%;"><center>หัวข้อ</center></th>
									<th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 10%;"><center>ราคา</center></th>
									<!-- <th>ส่วนลด</th> -->
									<!-- <th>ราคารวม</th> -->
									<th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 10%;"><center>รูปภาพ</center></th>
									<th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 10%;"><center>วันที่</center></th>
									<th class="sorting" tabindex="0" rowspan="1" colspan="1" style="width: 20%;"><center>Action</center></th>
								</tr>
							</thead>
							<?php $i = 1; foreach($course as $u ){
							
							?>
							
								<tr >
									<td><p><center><?php echo $i++ ?></center></p></td>
									<td><?php echo $u['fea_name']; ?></td>
									<!-- <td><?php
										$sss = $u['fea_des'];
										//print_r ($sss);
										$s = substr($sss,0,25);
										echo $s."...";
										?><br>
										<a data-toggle="modal" href="#myModal<?php echo $u['fea_id']?>" class="btn btn-primary btn-lg" id="<?php echo $u['fea_id']?>"><i class="fa fa-search"></i></a>
									</td> -->
									
									<!-- <td><span class="feature-detail"><?php echo $u['des_cor']; ?></td> -->
									<td><?php echo $u['fea_price']; ?></td>
									<!-- <td><?php if($u['dis_cor_b'] == 0){
										echo $u['dis_cor_per'].''.'%';
										
										}else{
										echo $u['dis_cor_b'].'บาท';
										
									} ?></td> -->
									<!-- <td><?php if($u['dis_cor_b'] == 0){
										$u['dis_cor_per'].''.'%';
										$val1 =$u['price_cor']- (($u['price_cor']*$u['dis_cor_per'])/100);
										echo $val1.'บาท';
										}else{
										$u['dis_cor_b'];
										$val = $u['price_cor']-$u['dis_cor_b'];
										echo $val.'บาท';
									} ?></td> -->
									<td><img id= "myImg" src="<?php echo base_url()."uploads/".$u['fea_img'];?>" alt="" class="img-thumbnail" width="150px" height="150px" data-toggle="modal" data-target="#enlargeImageModal<?php echo $u['fea_id']?>"/></td>
									<td><?php echo $u['fea_date']; ?></td>
									<td><center>
										<a data-toggle="modal" href="#myModal<?php echo $u['fea_id']?>" id="<?php echo $u['fea_id']?>"><button class="btn btn-info-alt" data-toggle="tooltip" title="ดูรายละเอียดเพิ่มเติม" data-placement="bottom"><i class="fa fa-search"></i></button></a>
										<!-- <?php
										if($u['fea_sta'] == '0'){ ?>
										
										<a href="update_status/<?php echo $u['fea_id'];?>/<?php echo $u['fea_sta'];?>" class="btn btn-midnightblue-alt" data-toggle="tooltip" title="เปิดการแสดง" data-placement="bottom" id = "slug<?php echo $u['fea_id'];?>"><i class="fa fa-eye-slash" name="status"></i></a>
										<?php }else{?>
										
										<a href="update_status/<?php echo $u['fea_id'];?>/<?php echo $u['fea_sta'];?>" class="btn btn-success-alt" data-toggle="tooltip" title="ปิดการแสดง" data-placement="bottom"  ><i id = "slug<?php echo $u['fea_id'];?>" class="fa fa-eye" name="status"></i></a>
										<?php }?> --> 
										
										<a href="edit_feature/<?php echo $u['fea_id'];?>" class="btn btn-warning-alt" data-toggle="tooltip" title="แก้ไขข้อมูล" data-placement="bottom"><i class="ti ti-pencil"></i></a>
										
										<a  class="btn btn-danger-alt delete"  id="<?php echo $u['fea_id'];?>" data-toggle="tooltip" title="ลบข้อมูล" data-placement="bottom" ><i class="ti ti-trash"></i></a>
										
										
									</center></td>
								</tr>
								
							
							
							<?php } ?>
						</table>
					</div>
					<div class="panel-footer"></div>
				</div>
			</div>
			<!--end code show data-->
			
			<!-- -->
		</div>
	</div>

<!--end table add -->
<?php $i = 1; foreach($course as $u ){

?>
<div class="modal fade" id="myModal<?php echo $u['fea_id']?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h2 class="modal-title">รายละเอียด ( <?php echo $u['fea_name'];?> ) </h2>
		</div>
		<div class="modal-body" >
			<?php echo $u['fea_id'];?>
			<?php echo $u['fea_des']; ?>
			<br>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close
			</button>
			
		</div>
		</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

		<!-- Modal images-->
							<div class="modal fade" id="enlargeImageModal<?php echo $u['fea_id']?>" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
					<div class="modal-body">
						<img src="<?php echo base_url()."uploads/".$u['fea_img'];?>" class="enlargeImageModalSource" style="width: 100%;">
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<!--table -->
		<!-- จบ -->
		</div> <!-- .container-fluid -->
		</div> <!-- #page-content -->
	</div>
		
		

	<footer role="contentinfo">
    <div class="clearfix">
        <ul class="list-unstyled list-inline pull-left">
            <li><h6 style="margin: 0;">&copy; 2016 Avenxo</h6></li>
        </ul>
        <button class="pull-right btn btn-link btn-xs hidden-print" id="back-to-top"><i class="ti ti-arrow-up"></i></button>
   	 </div>
	</footer>

</div>
</div>
</div>

<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-select2/select2.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-select2/select2.js"></script>  
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/datatables/jquery.dataTables.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>../avenxo/admin_html/assets/demo/demo-datatables.js"></script>
<!-- Datateble -->

<script type="text/javascript">
	 function readURL(input) {
	  $('#previewLoad').show();
	  if (input.files && input.files[0]) {
	    var reader = new FileReader();
	    var  name = input.files[0].name;

	    reader.onload = function (e) {
	      $('.image_preview').html('<img src="'+e.target.result+'" alt="'+reader.name+'" class="img-thumbnail" width="304" height="236"/>');
	    
	        $('.addFeature-img').val(name);
	    
	    }

	    reader.readAsDataURL(input.files[0]);
	    $('#previewLoad').hide();
	  }
	  }
	  
	  function reset(){
	  $('#image').val("");
	  $('.image_preview').html("");
	  }

	$(document).ready(function(){

		$(".summernote").summernote({
			 height: 200,
			 callbacks: {
    						onChange: function(contents, $editable) {
    							$("input#sum_desc").val(contents);
      							console.log('onChange:', contents, $editable);
      							chkform();
    						}
  			}

		});
		$('.e2').select2();

		

		var chkform = function(){
				var i = 0;
				$("input.addFeature-img").each(function(){
					if($(this).val()===''){
						i++;
					}
				})
				var txt = $('#sum_desc').val();
				var img = $("#image")[0].files.length;
				console.log('chk image :'+img)
				if(txt===''){
					i++;
				} if(img===0) {
					i++; 
				}
				console.log(i)
				if(i===0)
				{
					$("#btn-submit").prop('disabled',false)
				}else 
				{
					$("#btn-submit").prop('disabled',true)
				}
		}

		

		$(".addFeature").on('change keyup',chkform);
		$("#imgHolder").on('change click ',chkform);
		$(document).on('click', '#btn-submit', function(e) {
				e.preventDefault();
				swal({
						title: "ต้องการบันทึกข้อมูลใช่หรือไม่?",
						// text: 'Candidates are successfully shortlisted!',
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: '#00e600',
						confirmButtonText: 'บันทึก',
						cancelButtonText: "ยกเลิก",
						closeOnConfirm: false,
						closeOnCancel: false
				},
				function(isConfirm) {
					if (isConfirm) {
						swal({
								title: 'บันทึกข้อมูลเรียบร้อย',
								// text: 'Candidates are successfully shortlisted!',
								type: 'success'
						}, function() {
							$('#myForm').submit();
						});
					} else {
						swal("Cancelled", "", "error");
					}
				});
		});

		$(document).on('click', '.delete', function(e) {
				e.preventDefault();
				var id_delete = $(this).attr("id");
				swal({
						title: "ต้องการลบข้อมูลใช่หรือไม่?",
						// text: "You will not be able to recover this imaginary file!",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "ลบ",
						cancelButtonText: "ยกเลิก",
						closeOnConfirm: false,
						closeOnCancel: false
				},
				function(isConfirm){
					if (isConfirm) {
						$.ajax({
								url: "<?php echo base_url();?>index.php/welcome/delete_data",
								type: "POST",
								data: {'id_delete' : id_delete},
								dataType: "html",
								success: function () 
									{
										swal({
											title: 'ลบข้อมูลเรียบร้อย',
											// text: 'Candidates are successfully shortlisted!',
											type: 'success'
										}, function() {
											location.reload();
										});
									}
						});
					}else{
							swal("Cancelled", "Your imaginary file is safe :)", "error");
					}
				});
		});

		//visibility-on-off
		$('.btn-visibility').click(function() {
			if ($(this).hasClass('fa-eye')) {
				$(this).addClass('fa-eye-slash');
				$(this).addClass('show-off');
				$(this).removeClass('fa-eye');
			} else {
				$(this).addClass('fa-eye');
				$(this).removeClass('fa-eye-slash');
				$(this).removeClass('show-off');
			}
		});

		$('[data-toggle="tooltip"]').tooltip();

			$(function() {
				$('img').on('click', function() {
					$('.enlargeImageModalSource').attr('src', $(this).attr('src'));
					$('#enlargeImageModal').modal('show');
				});
			});
					

	});
</script>
</body>
</html>