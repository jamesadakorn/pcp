<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Sale Presentation</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-touch-fullscreen" content="yes">
    <meta name="description" content="Avenxo Admin Theme">
    <meta name="author" content="KaijuThemes">
     
    <link type='text/css' href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600' rel='stylesheet'>

    <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">        <!-- Font Awesome -->
    <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/fonts/themify-icons/themify-icons.css" rel="stylesheet">              <!-- Themify Icons -->
    <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/css/styles.css" rel="stylesheet">                                     <!-- Core CSS with all styles -->

    <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/codeprettifier/prettify.css" rel="stylesheet">                <!-- Code Prettifier -->
    <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/iCheck/skins/minimal/blue.css" rel="stylesheet">              <!-- iCheck -->

   <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-daterangepicker/daterangepicker-bs3.css" rel="stylesheet">    <!-- DateRangePicker -->

    <!--[if lt IE 10]>
        <script type="text/javascript" src="assets/js/media.match.min.js"></script>
        <script type="text/javascript" src="assets/js/respond.min.js"></script>
        <script type="text/javascript" src="assets/js/placeholder.min.js"></script>
    <![endif]-->
    <!-- The following CSS are included as plugins and can be removed if unused-->
    
        <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/fullcalendar/fullcalendar.css" rel="stylesheet"> 						<!-- FullCalendar -->
        <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/jvectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet"> 			<!-- jVectorMap -->
        <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/switchery/switchery.css" rel="stylesheet">   							<!-- Switchery -->
        <!-- <link type="text/css" href="<?php echo base_url();?>../bootstrap/dist/css/sweetalert2.min.css" rel="stylesheet"> -->
       <!--  <link type="text/css" href="<?php echo base_url();?>../bootstrap/dist/css/sweetalert2.css" rel="stylesheet"> -->
        <link type="text/css" href="<?php echo base_url();?>../bootstrap/dist/css/style.css" rel="stylesheet">
        <link type="text/css" href="<?php echo base_url();?>../bootstrap/dist/css/customize.css" rel="stylesheet">
        <link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/0.4.2/sweet-alert.min.css" />
       
       
        

                                                <!-- Multiselect -->

        <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-select2/select2.css" rel="stylesheet">                        <!-- Select2 -->
        <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-multiselect/css/multi-select.css" rel="stylesheet">           <!-- Multiselect -->
        <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/form-fseditor/fseditor.css" rel="stylesheet">                      <!-- FullScreen Editor -->
        <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/bootstrap-tokenfield/css/bootstrap-tokenfield.css" rel="stylesheet">   <!-- Tokenfield -->

        <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet"> <!-- Touchspin -->

        <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/iCheck/skins/minimal/_all.css" rel="stylesheet">                   <!-- Custom Checkboxes / iCheck -->
        <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/iCheck/skins/flat/_all.css" rel="stylesheet">
        <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/iCheck/skins/square/_all.css" rel="stylesheet">

        <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/card/lib/css/card.css" rel="stylesheet">                          <!-- Card -->
        <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/switchery/switchery.css" rel="stylesheet">                     <!-- Switchery -->     

         <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
          
          <link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet">
<link type="text/css" href="<?php echo base_url();?>../avenxo/admin_html/assets/plugins/datatables/dataTables.themify.css" rel="stylesheet">
 
         </head>

    <body class="animated-content">
        
        <header id="topnav" class="navbar navbar-default navbar-fixed-top" role="banner" style="background-color: #0066b2">


    <div class="logo-area" >
        <span id="trigger-sidebar" class="toolbar-trigger toolbar-icon-bg">
            <a data-toggle="tooltips" data-placement="right" title="Toggle Sidebar">
                <span class="icon-bg">
                    <i class="ti ti-menu"></i>
                </span>
            </a>
        </span>
        
        <div class="w3-white w3-xlarge">
    <div class="w3-padding-16 logo"> <a href="index" class="logo"> <i class="icon-c-logo hidden-sm hidden-md hidden-lg hidden-xl"> <img src="/bootstrap/dist/assets/images/rgb_logo_taradWhite.png" alt="TARAD.com" height="30"> </i> <span class="hidden-xs"><img src="/bootstrap/dist/assets/images/rgb_logo_taradWhite.png" alt="TARAD.com" height="50" style="margin-top: 0px"></span></a></div>
  </div>
    </div>
    <!-- logo-area -->

    <style>
.modal-image .modal-body {
    padding-top: 0;
    padding-bottom: 0;
}
.modal-image .modal-body .modal-img{
    margin: 0 -15px;
}
.modal-image .modal-body .list-group {
    margin: 0px -15px;
}
.modal-image .modal-body .list-group .list-group-item {
    background-color: transparent;
}
.modal-image .modal-body .list-group .list-group-item:first-child {
    border-radius:0;
}
.modal-image .modal-body .list-group .list-group-item:last-child {
    border: 0 none;
    border-radius: 0;
}

img {
    cursor: zoom-in;
}
</style>


 <!-- /* Style the tab */ -->
<style>
div.tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
div.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;
}

/* Change background color of buttons on hover */
div.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
div.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
}
</style>

</header>