

    <div class="widget" id="widget-progress">
    </div>
</div>
                    </div>
                </div>
                <div class="static-content-wrapper">
                    <div class="static-content">
                        <div class="page-content">
                            <ol class="breadcrumb">
                                
							<li class=""><a href="index.html">Home</a></li>
							<li class="active"><a href="index.html">Dashboard</a></li>

                            </ol>
                            <div class="container-fluid">
							
							<!-- เริ่ม -->
							
							<div class="panel panel-info" style="background-color: #00ffff">
								<div class="panel-heading">
									<h2>Edit Promotion</h2>
								</div>
								<div class="panel-editbox" data-widget-controls=""></div>
								<div class="panel-body">
								<!-- -->
									<div class="panel panel-default" data-widget='{"draggable": "false"}'>
										
										<div class="panel-editbox" data-widget-controls=""></div>
										<div class="panel-body" >
										<form action="<?php echo base_url('index.php/welcome/update_pro');?>" class="form-horizontal row-border" method="post" enctype="multipart/form-data">
											<?php $j = 1; foreach($promotion as $u ){ ?>
												<div class="form-group">
													<input type="hidden" name="pro_id" value="<?php echo $u['pro_id'];?>">
													<label class="col-sm-3 control-label">หัวข้อ</label>
													<div class="col-sm-6">

														<input name="pro_name" type="text" class="form-control" placeholder="หัวข้อ Promotion" required="" value="<?php echo $u['pro_name'];?>">
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">รายละเอียด</label>
													<div class="col-sm-6">
														<textarea name="pro_des" id="summernote" class="summernote" ><?php echo $u['pro_des'];?></textarea>
													</div>
												</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">วันที่เริ่ม - วันที่สิ้นสุด</label>
													<div class="col-sm-6">
														<div class="input-group" id="period">
															<span class="input-group-addon"><i class="ti ti-calendar"></i></span>
															    <input type="text" name="pro_start_date" class="actual_range" value="<?php echo $u['pro_start_date']?>">
															    <input type="text" name="pro_stop_date" class="actual_range" value="<?php echo $u['pro_stop_date']?>">
														</div>
													</div>
												</div>

													<div class="form-group">
														<label class="col-sm-3 control-label">Feature ที่เข้าร่วม</label>
														<div class="col-sm-6">
															<select  multiple style="width:100% !important" class="populate e2" name="feature[]" required="">
																<?php foreach ($course as $d){ ?>
																<option value="<?php echo $d['fea_id']?>"><?php echo $d['fea_name']?></option>
																<?php }?>
															</select>
														</div>
													</div>

													<div class="tab">
  														<button class="tablinks" onclick="openCity(event, 'baht')">ลดตามจำนวนบาท</button>
  														<button class="tablinks" onclick="openCity(event, 'percent')">ลดตามจำนวนเปอร์เซ็น</button>
  														<button class="tablinks" onclick="openCity(event, 'bahtpromo')">ราคาตามโปรโมชันหรือกำหนดราคาแถม</button>
 <!--   <button class="tablinks" onclick="openCity(event, 'Free')">กำหนดราคาแถม</button> -->
													</div>

													<div id="baht" class="tabcontent">
  														<div class="form-group">
						                        		<label class="col-md-3 control-label">ส่วนลดบาท : </label>  
						                       				<div class="input-group">
						                        			<input name="pro_dis_b" placeholder="0.00" class="form-control"  type="number" value="<?php echo $u['pro_dis_b']?>">
						                        			</div>
						                     			</div>
													</div>


													<div id="percent" class="tabcontent">
  														<div class="form-group">
						                        		<label class="col-md-3 control-label">ส่วนลด Percent : </label>  <div id="log"></div>	
						                        			<div class="input-group">
					                       	    			<input name="pro_dis_p" placeholder="0.00" class="form-control"  type="number" max="100.00" step="0.1" min="0.00" value="<?php echo $u['pro_dis_p']?>">
				                          					</div>
				                    		  			</div>
													</div>

													
													<div id="bahtpromo" class="tabcontent">
														<div class="form-group">
														<label class="col-sm-3 control-label">กำหนดราคา</label>
															<div class="col-sm-6">
															<input name="pro_total_price" type="number" class="form-control" placeholder="0.00" > <!-- required=""> -->
															</div>
														</div>

														<div class="form-group">
														<label class="col-sm-3 control-label">จำนวน Feature ที่ให้ลูกค้าเลือก <br><font color="red">**</font>หมายเหตุ ในกรณีที่แถมโปรโมชัน
													1 แถม 1 = 2 <br>,2 แถม 1 = 3</label>
															<div class="col-sm-6">
																<input name="pro_count" type="number" class="form-control" required="" value="<?php echo $u['pro_count']?>">
															</div>
														</div>
													</div>
													<!-- <div class="form-group">
													<label class="col-sm-3 control-label">จำนวน Feature ที่ให้ลูกค้าเลือก</label>
														<div class="col-sm-6">

															<input name="pro_count" type="number" class="form-control" required="" value="<?php echo $u['pro_count']?>">
														</div>
													</div>
												<div class="form-group">
						                        	<label class="col-md-3 control-label">ส่วนลด</label>  
						                        		<div class="col-md-4 inputGroupContainer">
						                        			<div class="col-sm-4"> 
						                        				<label class="radio-inline " for="radios-0">
						                  							<input type="radio" name="fruit" id="radios-0" >
						                            บาท
						                          				</label> 
						                          
						                       				 </div>
						                       	<div class="input-group">
						                        	<input name="pro_dis_b" placeholder="0.00" class="form-control"  type="number" value="<?php echo $u['pro_dis_b']?>">
						                        </div>
						                     			</div>
						                     	</div>

												<div class="form-group">
						                        	<label class="col-md-3 control-label">ส่วนลด</label>  
						                        		<div class="col-md-4 inputGroupContainer">
						                        			<div class="col-sm-4"> 
						                        				<label class="radio-inline " for="radios-0">
																	<input type="radio" name="fruit" id="radios-0"  >
						                            Percent
																</label> 
						                        <div id="log"></div>	
						                        			</div>
					                            	<div class="input-group">
					                       	    		<input name="pro_dis_p" placeholder="0.00" class="form-control"  type="number" max="100.00" step="0.1" min="0.00" value="<?php echo $u['pro_dis_p']?>">
				                          			</div>
				                    		  			</div>
				                      			</div>
												<div class="form-group">
													<label class="col-sm-3 control-label">ราคา</label>
													<div class="col-sm-6">

														<input name="pro_total_price" type="number" class="form-control" placeholder="0.00" required="" value="<?php echo $u['pro_total_price'];?>">
													</div>
												</div>	 -->

												<!-- Picture --> 
						                        <div class="form-group">
						                          <label class="col-sm-3 control-label" for="filebutton">รูปภาพ</label>
						                          <div class="col-md-6"> 
						                            <div class="input-group">
						                                  <span class="input-group-btn">
						                                      <span class="btn btn-default btn-file">
						                                          Browse… <input type="file" name="pro_img" required="">
						                                      </span>
						                                  </span>
						                                  <input type="text" class="form-control" readonly>
						                            </div>
						                          </div>
						                          <div class="row">
						                            <div class="col-md-12">
						                              <div class="col-md-6 col-md-offset-3">
						                                <img id='img-upload'/>
						                              </div>
						                            </div>
						                          </div> 
						                        </div>
										
												
											
										
										<div class="panel-footer">
											<div class="row">
												<div class="col-sm-offset-4">
													<button data-toggle="modal" href="#myModal" type="button" class="btn btn-warning-alt"><span class="ti ti-pencil-alt"> </span>Edit</button>
													<button type="reset" class="btn-default btn">Clear</button>
												</div>
											</div>
										</div>
									</div>
									<?php }?>
									</form>
								<!-- end code add feature -->
								</div>
							</div>
						</div>

						<!-- Modal -->
														<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
															<div class="modal-dialog">
																<div class="modal-content">
																	<div class="modal-header" align="center">
																		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																		<img src="<?php echo base_url('image/Achtung.jpg')?>" class="modal-title" width="100px" heigth="100px">
																	</div>
																	<div class="modal-body" align="center">
																		<h2>ต้องการแก้ไขข้อมูล Promotion ใช่หรือไม่</h2>

																	</div>
																	<div class="modal-footer">
																		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
																		<button type="submit" name="submit" class="btn-primary-alt btn saveja">Save</button>
																	</div>
																</div><!-- /.modal-content -->
															</div><!-- /.modal-dialog -->
														</div><!-- /.modal -->
							<!--end code show data-->
								
						<!--end table add -->
								
						<!--table -->

							<!-- จบ -->
                            </div> <!-- .container-fluid -->


                        </div> <!-- #page-content -->



                    </div>