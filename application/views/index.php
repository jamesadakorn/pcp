<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Index</title>

    <!-- Bootstrap -->
    <link href="../bootstrap-3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <!-- Navigation Bar-->
<div class="container">

    <form class="well form-horizontal" action=" " method="post"  id="contact_form">
<fieldset>

<!-- Form Name -->
<legend>Index</legend>

<div class="table-responsive pageview-tbproduct select-tb">
                  <table id="datatable" class="product-table table table-hover mails table table-actions-bar" data-tablesaw-sortable >
                    <thead>
                      <tr>
                        <th scope="col" style="width: 10%" data-tablesaw-sortable-col data-tablesaw-priority="1">ลำดับ</th>
                        <th scope="col" style="width: 10%" data-tablesaw-sortable-col data-tablesaw-priority="2">หัวข้อ</th>
                        <th scope="col" style="width: 10%;" data-tablesaw-sortable-col data-tablesaw-priority="3">รายละเอียด</th>
                        <th scope="col" style="width: 11%;" data-tablesaw-sortable-col data-tablesaw-priority="4">ราคา</th>
                        <th scope="col" style="width: 11%;" data-tablesaw-sortable-col data-tablesaw-priority="5">ส่วนลด</th>
                        <th scope="col" style="width: 12%;" data-tablesaw-sortable-col data-tablesaw-priority="6">ราคารวม</th>
                        <th scope="col" style="width: 12%; vertical-align: middle;" data-tablesaw-sortable-col data-tablesaw-priority="">Action</th>
                      </tr>
                    </thead>
                    <tbody class="list-product">
                      <tr>
                        <td>
                          <p>1</p>
                        </td>
                        <td>
                          <div class="media">
                            <div class="media-left">
                               <img class="media-objec product-img" src="assets/images/products/iphone.jpg">
                            </div>
                            <div class="media-body">
                              <p class="product-id">โปรสุดคุ้ม!!</p>
                            </div>
                          </div>
                        </td>
                        <td>พิเศษสำหรับลูกค้าที่บลาๆ</td>
                        <td>
                         <div class="col-sm-12">
                           <p class="product-id">9000</p>
                        </div>
                        </td>
                        <td><p class="product-id">40%</p></td>
                        <td>8500</td>
                        <td>
                          <a class="table-action-btn"><i class="glyphicon glyphicon-off"></i></a>
                          <a href="editfeature.html" class="table-action-btn"><i class="glyphicon glyphicon-pencil"></i></a>
                          <a href="#" class="table-action-btn"><i class="glyphicon glyphicon-remove"></i></a>
                        </td>
                      </tr>

                      <tr>
                        <td>
                          <p>2</p>
                        </td>
                        <td>
                          <div class="media">
                            <div class="media-left">
                               <img class="media-objec product-img" src="assets/images/products/iphone.jpg">
                            </div>
                            <div class="media-body">
                              <p class="product-id">โปรสุดคุ้ม!!</p>
                            </div>
                          </div>
                        </td>
                        <td>พิเศษสำหรับลูกค้าที่บลาๆ</td>
                        <td>
                         <div class="col-sm-12">
                           <p class="product-id">7000</p>
                        </div>
                        </td>
                        <td><p class="product-id">50%</p></td>
                        <td>3500</td>
                        <td>
                          <a class="table-action-btn"><i class="glyphicon glyphicon-off"></i></a>
                          <a href="editfeature.html" class="table-action-btn"><i class="glyphicon glyphicon-pencil"></i></a>
                          <a href="#" class="table-action-btn"><i class="glyphicon glyphicon-remove"></i></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

    

  </body>
</html>