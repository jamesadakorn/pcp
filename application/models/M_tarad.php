<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_tarad extends CI_Model {
	
		function get_result(){
			$this->db->select('*');
			$this->db->from('feature');
			$this->db->order_by('fea_id','ASC');
			$query = $this->db->get();
			return $query->result_array();
			
		}
		function edit_data($id,$data){
			$this->db->where('fea_id',$id);
			$this->db->update('feature',$data);

		}
		function show(){
			$this->db->select('*');
			$this->db->from('feature');
			$query = $this->db->get();
			return $query->result_array();
		}
		function insert($data){
			$this->db->insert('feature',$data);
			return true;
			
		}
	// 	public function delete_data($id){
		
	// 	$this->db->where('id', $id);
 //    	$this->db->delete('id_course');
    	
	// }
		public function Delete_data($id){
	    $this->db->where('fea_id',$id);
	    $this->db->delete('feature');
	   }
		public function SelectIdBycor($id){
			$this->db->select('*');
			$this->db->from('feature');
			$this->db->where('fea_id',$id);
			$query = $this->db->get();
			return $query->result_array();
		}
		

		//insert
		// public function insertB($data){
		// 	$this->db->insert('feature',$data);
		// }
		// public function insertPer($data){
		// 	$this->db->insert('feature',$data);
		// }
		// update status 11/9/17
		public function update_status($id,$data){
			$this->db->where('fea_id',$id);
			$this->db->update('feature',$data);
		}





		//function promotion
		function get_result_pro(){
			$this->db->select('*');
			$this->db->from('Promotion');
			$this->db->order_by('pro_id','ASC');
			$query = $this->db->get();
			return $query->result_array();
			
		}
		function get_result_fea_pro() {
			$sql = "SELECT promotion.pro_id,promotion.pro_name,promotion.pro_des,promotion.pro_dis_b,promotion.pro_dis_p,promotion.pro_img,promotion.pro_sta,promotion.pro_total_price,feature.*,GROUP_CONCAT(Feature.fea_name) AS fea_name FROM Promotion LEFT JOIN ID_Fea_Pro ON Promotion.pro_id = ID_Fea_Pro.id_pro LEFT JOIN Feature ON ID_Fea_Pro.id_fea = Feature.fea_id GROUP BY promotion.pro_id";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		function update_pro($data2,$data){
			try{
				$pro_id = (int)$data2['pro_id'];
				$this->db->where('pro_id',$pro_id);
				$this->db->update('Promotion',$data2);
				$this->db->query("DELETE FROM ID_Fea_Pro WHERE ID_Fea_Pro.id_pro = $pro_id;");
				$this->db->insert_batch('ID_Fea_Pro',$data);
				return true;

			}catch(Exception $e){
				return false;
			}
		}
		function get_status() {
			$sql = "SELECT promotion.*,feature.*,GROUP_CONCAT(Feature.fea_name) AS fea_name FROM Promotion LEFT JOIN ID_Fea_Pro ON Promotion.pro_id = ID_Fea_Pro.id_pro LEFT JOIN Feature ON ID_Fea_Pro.id_fea = Feature.fea_id WHERE Promotion.pro_sta = 0 AND Promotion.pro_start_date < Promotion.pro_stop_date AND promotion.timestamp < promotion.pro_stop_date GROUP BY promotion.pro_id";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		public function selectById_pro($pro_id){
			$sql = "SELECT promotion.* ,GROUP_CONCAT(Feature.fea_name) AS fea_name FROM Promotion LEFT JOIN ID_Fea_Pro ON promotion.pro_id = ID_Fea_Pro.id_pro LEFT JOIN Feature ON ID_Fea_Pro.id_fea = Feature.fea_id WHERE promotion.pro_id = $pro_id GROUP BY promotion.pro_id";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
	
		public function insertPro2($data){
			$this->db->insert_batch('ID_Fea_Pro',$data);
		}
		public function insertPro($data2){
			$sql = "INSERT INTO Promotion(`pro_name`,`pro_des`,`pro_img`,`pro_dis_b`,`pro_dis_p`,`pro_total_price`,`pro_count`,`pro_start_date`,`pro_stop_date`) VALUES('".$data2['pro_name']."','".$data2['pro_des']."','".$data2['pro_img']."','".$data2['pro_dis_b']."','".$data2['pro_dis_p']."','".$data2['pro_total_price']."','".$data2['pro_count']."','".$data2['pro_start_date']."','".$data2['pro_stop_date']."');";
			if($this->db->query($sql)){
				return $this->db->query("SELECT LAST_INSERT_ID() as pro_id")->row_array();
			}else{
				return false;
			}
		}
		public function update_sta($pro_id,$data){
			$this->db->where('pro_id',$pro_id);
			$this->db->update('Promotion',$data);
		}
		public function show_det_p($pro_id){
			$sql = "SELECT promotion.*,feature.* ,GROUP_CONCAT(Feature.fea_name) AS fea_name FROM Promotion LEFT JOIN ID_Fea_Pro ON promotion.pro_id = ID_Fea_Pro.id_pro LEFT JOIN Feature ON ID_Fea_Pro.id_fea = Feature.fea_id WHERE promotion.pro_id = $pro_id ";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		public function get_result_fea_pro_f($id) {
			$sql = "SELECT promotion.pro_id,promotion.pro_name,promotion.pro_count,promotion.pro_dis_p,promotion.pro_dis_b,promotion.pro_total_price,GROUP_CONCAT(Feature.fea_name) AS fea_name,GROUP_CONCAT(feature.fea_price) AS fea_price FROM Promotion LEFT JOIN ID_Fea_Pro ON Promotion.pro_id = ID_Fea_Pro.id_pro LEFT JOIN Feature ON ID_Fea_Pro.id_fea = Feature.fea_id  WHERE promotion.pro_id = $id  ";
			$query = $this->db->query($sql);
			return $query->row_array();
		}
		//////////////////////////////////////////////reportshop
		//reportshop//////////////////////////////////////////////////////////////////////////////////////////////////
		public function get_result_shop_pro(){
			$sql = "SELECT reportshop.shop_id,reportshop.shop_url,reportshop.shop_owner_name,reportshop.shop_tel,reportshop.shop_email,reportshop.shop_pro,reportshop.shop_date,promotion.pro_name as pro_name, GROUP_CONCAT(Feature.fea_name) AS fea_name,GROUP_CONCAT(Feature.fea_price) AS fea_price FROM reportshop LEFT JOIN id_shop_fea ON reportshop.shop_id = id_shop_fea.id_shop LEFT JOIN feature ON id_shop_fea.id_fea = feature.fea_id LEFT JOIN promotion ON promotion.pro_id = reportshop.shop_pro GROUP by reportshop.shop_id" ;
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		public function get_result_shop_prox(){
			$sql = "SELECT reportshop.shop_id,reportshop.shop_url,reportshop.shop_owner_name,reportshop.shop_tel,reportshop.shop_email,reportshop.shop_pro,reportshop.shop_date,reportshop.shop_dis_b,reportshop.shop_dis_p,reportshop.shop_total_price,promotion.pro_name as pro_name, GROUP_CONCAT(Feature.fea_name) AS fea_name,GROUP_CONCAT(Feature.fea_price) AS fea_price FROM reportshop LEFT JOIN id_shop_fea ON reportshop.shop_id = id_shop_fea.id_shop LEFT JOIN feature ON id_shop_fea.id_fea = feature.fea_id LEFT JOIN promotion ON promotion.pro_id = reportshop.shop_pro GROUP by reportshop.shop_id" ;
			$query = $this->db->query($sql);
			return $query->result_array();
		}
		public function insertShop($data3){
			$sql = "INSERT INTO reportshop(`shop_url`,`shop_owner_name`,`shop_tel`,`shop_email`,`shop_date`,`shop_pro`,`shop_dis_b`,`shop_dis_p`,`shop_total_price`) VALUES('".$data3['shop_url']."','".$data3['shop_owner_name']."','".$data3['shop_tel']."','".$data3['shop_email']."' , '".$data3['shop_date']."' , '".$data3['shop_pro']."', '".$data3['shop_dis_b']."', '".$data3['shop_dis_p']."','".$data3['shop_total_price']."');";
			if($this->db->query($sql)){
				return $this->db->query("SELECT LAST_INSERT_ID() as shop_id")->row_array();
			}else{
				return false;
			}

			// $this->db->insert('ID_Fea_Pro',$data2);
			//$result = $this->db->query($sql)->row_array();
		}

		public function insertReportShop($data1){
			$this->db->insert('reportshop',$data1);
			if($this->db->query($data)){
				return $this->db->query("SELECT LAST_INSERT_ID() as shop_id")->row_array();
			}else{
				return false;
			}
		}
		public function insertShop2($data){
			$this->db->insert_batch('id_shop_fea',$data);
		}
		function get_result_shop(){
			$this->db->select('*');
			$this->db->from('reportshop');
			$this->db->order_by('shop_id','ASC');
			$query = $this->db->get();
			return $query->result_array();
			
		}
		function get_result_fea_pro2($id) {
			$sql = "SELECT promotion.pro_id,promotion.pro_name,GROUP_CONCAT(Feature.fea_name) AS fea_name,GROUP_CONCAT(Feature.fea_price) AS fea_price,GROUP_CONCAT(Feature.fea_id) AS fea_id  ,promotion.pro_dis_b as dis_b, promotion.pro_dis_p as dis_p ,promotion.pro_total_price as pro_total FROM Promotion LEFT JOIN ID_Fea_Pro ON Promotion.pro_id = ID_Fea_Pro.id_pro LEFT JOIN Feature ON ID_Fea_Pro.id_fea = Feature.fea_id  WHERE promotion.pro_id = $id  ";
			$query = $this->db->query($sql);
			return $query->row_array();
		}
		public function get_report(){
			$this->db->select('*');
			$this->db->from('reportshop');
			$query = $this->db->get();
			return $query->result_array();
		}
		function get_status2() {
			$sql = "SELECT promotion.*,feature.*,GROUP_CONCAT(Feature.fea_name) AS fea_name FROM Promotion LEFT JOIN ID_Fea_Pro ON Promotion.pro_id = ID_Fea_Pro.id_pro LEFT JOIN Feature ON ID_Fea_Pro.id_fea = Feature.fea_id WHERE Promotion.pro_sta = 0 AND Promotion.pro_start_date < Promotion.pro_stop_date GROUP BY promotion.pro_id";
			$query = $this->db->query($sql);
			return $query->result_array();
		}
	
	}
    