$( document ).ready(function() {
    
	$('.owl-carousel').owlCarousel({
		loop:true,
		margin:0,
		items:1,
		nav:true,
		autoplay:true,
    	autoplayTimeout:3000,
		smartSpeed:1000
	});
	
		// Custom 
	  var stickyToggle = function(sticky, stickyWrapper, scrollElement) {
		var stickyHeight = sticky.outerHeight();
		var stickyTop = stickyWrapper.offset().top;
		if (scrollElement.scrollTop() >= stickyTop){
		  stickyWrapper.height(stickyHeight);
		  sticky.addClass("is-sticky");
		}
		else{
		  sticky.removeClass("is-sticky");
		  stickyWrapper.height('auto');
		}
	  };
	  
	  // Find all data-toggle="sticky-onscroll" elements
	  $('[data-toggle="sticky-onscroll"]').each(function() {
		var sticky = $(this);
		var stickyWrapper = $('<div>').addClass('sticky-wrapper'); // insert hidden element to maintain actual top offset on page
		sticky.before(stickyWrapper);
		sticky.addClass('sticky');
		
		// Scroll & resize events
		$(window).on('scroll.sticky-onscroll resize.sticky-onscroll', function() {
		  stickyToggle(sticky, stickyWrapper, $(this));
		});
		
		// On page load
		stickyToggle(sticky, stickyWrapper, $(window));
	  });
  

	
    //stick in the fixed 100% height behind the navbar but don't wrap it
    $('#slide-nav.navbar .container').append($('<div id="navbar-height-col"></div>'));

    // Enter your ids or classes
    var toggler = '.navbar-toggle';
    var pagewrapper = '#page-content';
    var navigationwrapper = '.navbar-header';
    var menuwidth = '100%'; // the menu inside the slide menu itself
    var slidewidth = '100%';
    var menuneg = '-100%';
    var slideneg = '-100%';


    $("#slide-nav").on("click", toggler, function (e) {

        var selected = $(this).hasClass('slide-active');

        $('#slidemenu').stop().animate({
            left: selected ? menuneg : '0px'
        });

        $('#navbar-height-col').stop().animate({
            left: selected ? slideneg : '0px'
        });

        $(pagewrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });

        $(navigationwrapper).stop().animate({
            left: selected ? '0px' : slidewidth
        });


        $(this).toggleClass('slide-active', !selected);
        $('#slidemenu').toggleClass('slide-active');


        $('#page-content, .navbar, body, .navbar-header').toggleClass('slide-active');


    });


    var selected = '#slidemenu, #page-content, body, .navbar, .navbar-header';


    $(window).on("resize", function () {

        if ($(window).width() > 767 && $('.navbar-toggle').is(':hidden')) {
            $(selected).removeClass('slide-active');
        }


    });	
	
	
	
	
	$('#contact_form').bootstrapValidator({
        
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                validators: {
                        stringLength: {
						message: 'กรุณากรอกข้อมูลให้ครบถ้วน'	,
                        min: 2,
                    },
                        notEmpty: {
                        message: 'กรุณากรอกข้อมูลให้ครบถ้วน'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'กรุณากรอกข้อมูลให้ครบถ้วน'
                    },
                    emailAddress: {
                        message: 'กรุณากรอกข้อมูลให้ครบถ้วน'
                    }
                }
            },
            service: {
                validators: {
                    notEmpty: {
                        message: 'กรุณาเลือกบริการที่สนใจ'
                    }
                }
            },


            phone: {
                validators: {
                        stringLength: {
                        message: 'กรุณากรอกข้อมูลให้ครบถ้วน'    ,
                        min: 10,
                    },
                        notEmpty: {
                        message: 'กรุณากรอกข้อมูลให้ครบถ้วน'
                    }
                }
            },
            phone1: {
                validators: {
                        stringLength: {
                        message: 'กรุณากรอกข้อมูลให้ครบถ้วน'    ,
                        min: 9,
                    },
                        notEmpty: {
                        message: 'กรุณากรอกข้อมูลให้ครบถ้วน'
                    }
                }
            },
            lastname: {
                validators: {
                        stringLength: {
                        message: 'กรุณากรอกข้อมูลให้ครบถ้วน'    ,
                        min: 2,
                    },
                        notEmpty: {
                        message: 'กรุณากรอกข้อมูลให้ครบถ้วน'
                    }
                }
            },
            province: {
                validators: {
                    notEmpty: {
                        message: 'กรุณาเลือกข้อมูลให้ครบถ้วน'
                    },
                }
            },
            birthday: {
                validators: {
                    notEmpty: {
                        message: 'กรุณากรอกข้อมูลให้ครบถ้วน'
                    },
                }
            },
            id: {
                validators: {
                        stringLength: {
                        message: 'กรุณากรอกข้อมูลให้ครบถ้วน'    ,
                        min: 13,
                    },
                        notEmpty: {
                        message: 'กรุณากรอกข้อมูลให้ครบถ้วน'
                    }
                }
            },
            career: {
                validators: {
                    notEmpty: {
                        message: 'กรุณาเลือกข้อมูลให้ครบถ้วน'
                    },
                }
            },
            url: {
                validators: {
                    notEmpty: {
                        message: 'กรุณากรอก url ของร้านค้า'
                    },
                }
            },
            username: {
                validators: {
                        stringLength: {
                        message: 'กรุณากรอกข้อมูลให้มีความยาว 8 - 16 ตัวอักษร'    ,
                        min: 8,
                    },
                        notEmpty: {
                        message: 'กรุณากรอกข้อมูลให้มีความยาว 8 - 16 ตัวอักษร'
                    }
                }
            },
            password: {
                validators: {
                        stringLength: {
                        message: 'กรุณากรอกข้อมูลให้มีความยาว 8 - 15 ตัวอักษร ไม่เว้นวรรค'    ,
                        min: 8,
                    },
                        notEmpty: {
                        message: 'กรุณากรอกข้อมูลให้มีความยาว 8 - 15 ตัวอักษร ไม่เว้นวรรค'
                    }
                }
            },
            confirmPassword: {
                validators: {
                        stringLength: {
                        message: 'กรุณากรอกข้อมูลให้มีความยาว 8 - 15 ตัวอักษร ไม่เว้นวรรค'    ,
                        min: 8,
                    },
                        notEmpty: {
                        message: 'กรุณากรอกข้อมูลให้มีความยาว 8 - 15 ตัวอักษร ไม่เว้นวรรค'
                    }
                }
            },
            store: {
                validators: {
                    notEmpty: {
                        message: 'กรุณาเลือกข้อมูลให้ครบถ้วน'
                    },
                }
            }
		}
        })
        .on('success.form.bv', function(e) {
            $('#success_message').slideDown({ opacity: "show" }, "slow") // Do something ...
                $('#contact_form').data('bootstrapValidator').resetForm();

            // Prevent form submission
            e.preventDefault();

            // Get the form instance
            var $form = $(e.target);

            // Get the BootstrapValidator instance
            var bv = $form.data('bootstrapValidator');

            // Use Ajax to submit form data
            $.post($form.attr('action'), $form.serialize(), function(result) {
                console.log(result);
            }, 'json');
        });
	
	  

});





var sections = $('section')
  , nav = $('.nav-scroll')
  , nav_height = nav.outerHeight();

$(window).on('scroll', function () {
  var cur_pos = $(this).scrollTop();
  
  sections.each(function() {
    var top = $(this).offset().top - nav_height,
        bottom = top + $(this).outerHeight();
    
    if (cur_pos >= top && cur_pos <= bottom) {
      nav.find('a').removeClass('active');
      sections.removeClass('active');
      
      $(this).addClass('active');
      nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
    }
  });
});

nav.find('a').on('click', function () {
  var $el = $(this)
    , id = $el.attr('href');
  
  $('html, body').animate({
    scrollTop: $(id).offset().top - nav_height
  }, 500);
  
  return false;
});
